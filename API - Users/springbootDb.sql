-- MySQL dump 10.13  Distrib 8.0.34, for Win64 (x86_64)
--
-- Host: localhost    Database: springboot
-- ------------------------------------------------------
-- Server version	8.0.34

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `client`
--

DROP TABLE IF EXISTS `client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `client` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `direction` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `priority` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client`
--

LOCK TABLES `client` WRITE;
/*!40000 ALTER TABLE `client` DISABLE KEYS */;
INSERT INTO `client` VALUES (1,'Lima - Peru','Juan123@gmail.com','Juan','987-456-123',5),(2,'Bogotá - Colombia','Maria456@hotmail.com','María','555-123-789',4),(3,'Santiago - Chile','Carlos789@yahoo.com','Carlos','555-987-456',3),(4,'Buenos Aires - Argentina','Laura567@gmail.com','Laura','555-456-789',2),(5,'Quito - Ecuador','Andres321@gmail.com','Andrés','555-789-123',4),(6,'Lima - Perú','Ana789@hotmail.com','Ana','555-123-456',3),(7,'São Paulo - Brasil','Diego123@gmail.com','Diego','555-321-987',5),(8,'Caracas - Venezuela','Marta456@yahoo.com','Marta','555-987-789',2),(9,'La Paz - Bolivia','Pedro789@hotmail.com','Pedro','555-456-123',3),(10,'Montevideo - Uruguay','Isabel321@gmail.com','Isabel','555-123-789',1),(11,'Asunción - Paraguay','Rafael567@hotmail.com','Rafael','555-789-456',4);
/*!40000 ALTER TABLE `client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `precio` double NOT NULL,
  `priority` int DEFAULT NULL,
  `stock` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,'una gaseosa gasificada','Coca Cola',3.99,1,100),(2,'Una bebida gaseosa refrescante','Pepsi',3.49,2,120),(3,'Bebida gaseosa con sabor a limón-lima','Sprite',2.99,3,80),(4,'Bebida gaseosa con sabores de frutas','Fanta',4.25,1,90),(5,'Bebida gaseosa sin azúcar','Coca Cola Zero',3.99,2,60),(6,'Bebida gaseosa con sabor único','Dr. Pepper',4.49,3,75),(7,'Bebida gaseosa con sabor a cítricos','Mountain Dew',3.79,2,110),(8,'Bebida gaseosa con sabor a lima-limón','7UP',3.49,3,95),(9,'Tónica para mezclar con bebidas espirituosas','Schweppes Tónica',2.99,1,70),(10,'Bebida gaseosa sin azúcar ni calorías','Pepsi Max',3.49,2,85),(11,'Refresco de jengibre con burbujas','Ginger Ale Canada Dry',4.29,3,55);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proyect`
--

DROP TABLE IF EXISTS `proyect`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `proyect` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `dateend` datetime(6) DEFAULT NULL,
  `dateinit` datetime(6) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `priority` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proyect`
--

LOCK TABLES `proyect` WRITE;
/*!40000 ALTER TABLE `proyect` DISABLE KEYS */;
INSERT INTO `proyect` VALUES (1,'2023-09-29 19:00:00.000000','2023-09-13 19:00:00.000000','informacion sobre pokemones','PokeApi',3),(2,'2023-09-29 19:00:00.000000','2023-09-13 19:00:00.000000','API para obtener noticias de todo el mundo','NewsAPI',2),(3,'2023-09-29 19:00:00.000000','2023-09-13 19:00:00.000000','API para consultar el pronóstico del tiempo','WeatherAPI',1),(4,'2023-09-29 19:00:00.000000','2023-09-13 19:00:00.000000','API para obtener información de ubicación geográfica','LocationAPI',2),(5,'2023-09-29 19:00:00.000000','2023-09-13 19:00:00.000000','API para traducir texto a varios idiomas','TranslationAPI',3),(6,'2023-09-29 19:00:00.000000','2023-09-13 19:00:00.000000','API para obtener cotizaciones de acciones en tiempo real','StockQuotesAPI',2),(7,'2023-10-14 19:00:00.000000','2023-09-30 19:00:00.000000','API para gestionar eventos y calendarios','EventCalendarAPI',2),(8,'2023-10-04 19:00:00.000000','2023-09-19 19:00:00.000000','API para realizar conversiones de divisas','CurrencyConversionAPI',1),(9,'2023-10-09 19:00:00.000000','2023-09-21 19:00:00.000000','API para gestionar reservas de hoteles','HotelBookingAPI',3),(10,'2023-10-11 19:00:00.000000','2023-09-24 19:00:00.000000','API para interactuar con plataformas de redes sociales','SocialMediaAPI',2),(11,'2023-10-19 19:00:00.000000','2023-10-04 19:00:00.000000','API para analizar sentimientos en texto','SentimentAnalysisAPI',1);
/*!40000 ALTER TABLE `proyect` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service`
--

DROP TABLE IF EXISTS `service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `service` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `price` double NOT NULL,
  `priority` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service`
--

LOCK TABLES `service` WRITE;
/*!40000 ALTER TABLE `service` DISABLE KEYS */;
INSERT INTO `service` VALUES (1,'Descripción del servicio','Nombre del servicio',9.99,20),(2,'Cambio de pantalla','Mantenimiento de Celulares',15,2),(3,'Reparación de hardware y software','Reparación de Computadoras',50,3),(4,'Limpieza profunda de alfombras','Limpieza de Alfombras',25,2),(5,'Corte y mantenimiento de jardines','Corte de Césped',30,1),(6,'Reparación de tuberías y grifos','Servicio de Fontanería',40,2),(7,'Limpieza profunda de todas las áreas de la casa','Limpieza del Hogar',35,2),(8,'Clases de yoga para principiantes y avanzados','Clases de Yoga',20,1),(9,'Reparación de lavadoras, neveras y más','Reparación de Electrodomésticos',45,3),(10,'Pintura de paredes y techos en interiores','Pintura de Interiores',55,2),(11,'Entrenador personal para lograr tus objetivos de fitness','Entrenamiento Personalizado',60,1),(12,'Reparaciones urgentes de tuberías y desagües','Plomería de Emergencia',75,3);
/*!40000 ALTER TABLE `service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `priority` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'nicolas@gmail.com','Nicolas',1),(2,'alexis@gmail.com','Alexis',2),(3,'juan@gmail.com','Juan',3),(4,'maria@gmail.com','María',4),(5,'carlos@gmail.com','Carlos',5),(6,'ana@gmail.com','Ana',6),(7,'david@gmail.com','David',7),(8,'laura@gmail.com','Laura',8),(9,'pedro@gmail.com','Pedro',9),(10,'sara@gmail.com','Sara',10),(11,'eduardo@gmail.com','Eduardo',11),(12,'isabel@gmail.com','Isabel',12),(13,'hector@gmail.com','Héctor',13);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-09-14  3:38:52
