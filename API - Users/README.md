# API Local de Usuarios con MySQL

Esta API local de usuarios con MySQL es una aplicación sencilla que te permite gestionar la información de los usuarios, incluyendo su ID, nombre, correo y email. Puedes utilizar esta API para agregar y borrar usuarios mediante Postman u otras herramientas similares. [Desarrollada por mi]()!

## Requisitos Previos

Asegúrate de tener instalados los siguientes requisitos previos antes de utilizar esta API:

- [ ] MySQL Server: Debes tener un servidor MySQL configurado y en ejecución en tu máquina local o en un servidor remoto.
- [ ] Postman: Para probar las solicitudes HTTP a la API.
- [ ] En el codigo dejo la base de datos exportada para que se pueda probar



## Uso
Para agregar un nuevo usuario, realiza una solicitud POST a la siguiente URL:

```
http://localhost:8080/api/usuario
```
Asegúrate de enviar los datos del usuario en el cuerpo de la solicitud en formato JSON. Por ejemplo:

```
{
    "name" : "name",
    "email" : "email",
    "priority" : 1
}
```
## Borrar un Usuario
Para borrar un usuario existente, realiza una solicitud DELETE a la siguiente URL, especificando el ID del usuario que deseas eliminar:

```
http://localhost:8080/api/usuario/{id}
```
## Consultas 
```
{
    http://localhost:8080/user
}
```
```
{
    http://localhost:8080/user/1
}
```
```
{
    http://localhost:8080/user/query?priority=1
}
```
```
{
    http://localhost:8080/product
}
```
```
{
    http://localhost:8080/service
}
```
```
{
    http://localhost:8080/client
}
```
```
{
    http://localhost:8080/service
}
```
## Gracias por tu visita

