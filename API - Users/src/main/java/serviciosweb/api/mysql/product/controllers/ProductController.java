package serviciosweb.api.mysql.product.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import serviciosweb.api.mysql.product.interfaces.ProductInterface;
import serviciosweb.api.mysql.product.models.ProductModel;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/product")
public class ProductController {
    private final ProductInterface productInterface;

    @Autowired
    public ProductController(ProductInterface productInterface) {
        this.productInterface = productInterface;
    }

    @GetMapping
    public List<ProductModel> obtainProduct(){return productInterface.obtainProduct();}

    @PostMapping
    public ProductModel saveProduct(@RequestBody ProductModel product){
        return productInterface.saveProduct(product);
    }

    @GetMapping(path = "/{id}")
    public Optional<ProductModel> obtainProductForId(@PathVariable("id") Long id){
        return productInterface.obtainForId(id);
    }

    @GetMapping("/query")
    public List<ProductModel> obtainProductForPriority(@RequestParam("priority") Integer priority){
        return productInterface.obtainForPriority(priority);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> eliminateForId(@PathVariable("id") Long id){
        boolean eliminate = productInterface.eliminateProduct(id);
        if (eliminate) {
            return ResponseEntity.ok("Product with ID was deleted " + id);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Could not find or delete user with ID " + id);
        }
    }
}
