package serviciosweb.api.mysql.product.interfaces;

import serviciosweb.api.mysql.product.models.ProductModel;

import java.util.ArrayList;
import java.util.Optional;

public interface ProductInterface {
    ArrayList<ProductModel> obtainProduct();
    ProductModel saveProduct(ProductModel product);
    Optional<ProductModel> obtainForId(Long id);
    ArrayList<ProductModel> obtainForPriority(Integer prioridad);
    boolean eliminateProduct(Long id);
}
