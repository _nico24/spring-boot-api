package serviciosweb.api.mysql.product.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import serviciosweb.api.mysql.product.interfaces.ProductInterface;
import serviciosweb.api.mysql.product.models.ProductModel;
import serviciosweb.api.mysql.product.repositories.ProductRepository;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class ProductService implements ProductInterface {
    private final ProductRepository productService;

    @Autowired
    public ProductService(ProductRepository productService) {
        this.productService = productService;
    }

    @Override
    public ArrayList<ProductModel> obtainProduct() {
        return (ArrayList<ProductModel>) productService.findAll();
    }

    @Override
    public ProductModel saveProduct(ProductModel product) {
        return productService.save(product);
    }

    @Override
    public Optional<ProductModel> obtainForId(Long id) {
        return productService.findById(id);
    }

    @Override
    public ArrayList<ProductModel> obtainForPriority(Integer priority) {
        return productService.findByPriority(priority);
    }

    @Override
    public boolean eliminateProduct(Long id) {
        try {
            productService.deleteById(id);
            return true;
        } catch (Exception err) {
            return false;
        }
    }
}
