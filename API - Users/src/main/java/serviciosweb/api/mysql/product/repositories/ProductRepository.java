package serviciosweb.api.mysql.product.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import serviciosweb.api.mysql.product.models.ProductModel;

import java.util.ArrayList;

@Repository
public interface ProductRepository extends CrudRepository<ProductModel, Long> {
    ArrayList<ProductModel> findByPriority(Integer priority);
}
