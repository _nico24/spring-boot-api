package serviciosweb.api.mysql.client.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import serviciosweb.api.mysql.client.interfaces.ClientInterface;
import serviciosweb.api.mysql.client.models.ClientModel;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/client")
public class ClientController {
    private final ClientInterface clientInterface;

    @Autowired
    public ClientController(ClientInterface clientInterface) {
        this.clientInterface = clientInterface;
    }

    @GetMapping
    public List<ClientModel> obtainClient(){return clientInterface.obtainClient();}

    @PostMapping
    public ClientModel saveClient(@RequestBody ClientModel client){
        return clientInterface.saveClient(client);
    }

    @GetMapping(path = "/{id}")
    public Optional<ClientModel> obtainClientForId(@PathVariable("id") Long id){
        return clientInterface.obtainForId(id);
    }

    @GetMapping("/query")
    public List<ClientModel> obtainClientForPriority(@RequestParam("priority") Integer priority){
        return clientInterface.obtainForPriority(priority);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> eliminateForId(@PathVariable("id") Long id){
        boolean eliminate = clientInterface.eliminateClient(id);
        if (eliminate) {
            return ResponseEntity.ok("Client with ID was deleted " + id);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Could not find or delete user with ID " + id);
        }
    }
}
