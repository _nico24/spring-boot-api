package serviciosweb.api.mysql.client.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import serviciosweb.api.mysql.client.models.ClientModel;

import java.util.ArrayList;

@Repository
public interface ClientRepository extends CrudRepository<ClientModel, Long> {
    ArrayList<ClientModel> findByPriority(Integer priority);
}
