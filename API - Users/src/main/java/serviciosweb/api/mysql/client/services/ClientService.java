package serviciosweb.api.mysql.client.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import serviciosweb.api.mysql.client.interfaces.ClientInterface;
import serviciosweb.api.mysql.client.models.ClientModel;
import serviciosweb.api.mysql.client.repositories.ClientRepository;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class ClientService implements ClientInterface {
    private final ClientRepository clientService;

    @Autowired
    public ClientService(ClientRepository clientRepository) {
        this.clientService = clientRepository;
    }

    @Override
    public ArrayList<ClientModel> obtainClient() {
        return (ArrayList<ClientModel>) clientService.findAll();
    }

    @Override
    public ClientModel saveClient(ClientModel client) {
        return clientService.save(client);
    }

    @Override
    public Optional<ClientModel> obtainForId(Long id) {
        return clientService.findById(id);
    }

    @Override
    public ArrayList<ClientModel> obtainForPriority(Integer client) {
        return clientService.findByPriority(client);
    }

    @Override
    public boolean eliminateClient(Long id) {
        try {
            clientService.deleteById(id);
            return true;
        } catch (Exception err) {
            return false;
        }
    }
}
