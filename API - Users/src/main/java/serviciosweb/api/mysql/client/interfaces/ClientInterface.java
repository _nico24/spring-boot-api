package serviciosweb.api.mysql.client.interfaces;

import org.springframework.stereotype.Service;
import serviciosweb.api.mysql.client.models.ClientModel;

import java.util.ArrayList;
import java.util.Optional;

@Service
public interface ClientInterface {
    ArrayList<ClientModel> obtainClient();
    ClientModel saveClient(ClientModel client);
    Optional<ClientModel> obtainForId(Long id);
    ArrayList<ClientModel> obtainForPriority(Integer client);
    boolean eliminateClient(Long id);
}
