package serviciosweb.api.mysql.service.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import serviciosweb.api.mysql.service.interfaces.ServiceInterface;
import serviciosweb.api.mysql.service.models.ServiceModel;
import serviciosweb.api.mysql.service.repositories.ServiceRepository;

import java.util.ArrayList;
import java.util.Optional;


@Service
public class ServiceServices implements ServiceInterface {
    private final ServiceRepository serviceService;

    @Autowired
    public ServiceServices(ServiceRepository serviceService) {
        this.serviceService = serviceService;
    }

    @Override
    public ArrayList<ServiceModel> obtainService() {
        return (ArrayList<ServiceModel>) serviceService.findAll();
    }

    @Override
    public ServiceModel saveService(ServiceModel service) {
        return serviceService.save(service);
    }

    @Override
    public Optional<ServiceModel> obtainForId(Long id) {
        return serviceService.findById(id);
    }

    @Override
    public ArrayList<ServiceModel> obtainForPriority(Integer priority) {
        return serviceService.findByPriority(priority);
    }

    @Override
    public boolean eliminateService(Long id) {
        try {
            serviceService.deleteById(id);
            return true;
        } catch (Exception err){
            return false;
        }
    }
}
