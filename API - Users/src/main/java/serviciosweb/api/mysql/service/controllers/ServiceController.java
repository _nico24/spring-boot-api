package serviciosweb.api.mysql.service.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import serviciosweb.api.mysql.service.interfaces.ServiceInterface;
import serviciosweb.api.mysql.service.models.ServiceModel;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/service")
public class ServiceController {
    private final ServiceInterface serviceInterface;

    @Autowired
    public ServiceController(ServiceInterface serviceInterface) {
        this.serviceInterface = serviceInterface;
    }

    @GetMapping
    public List<ServiceModel> obtainService(){return serviceInterface.obtainService();}

    @PostMapping
    public ServiceModel saveService(@RequestBody ServiceModel service){
        return serviceInterface.saveService(service);}

    @GetMapping(path = "/{id}")
    public Optional<ServiceModel> obtainForId(@PathVariable("id")Long id){return  serviceInterface.obtainForId(id);}

    @GetMapping("/query")
    public List<ServiceModel> obtainServiceForPriority(@RequestParam("priority") Integer priority){
        return serviceInterface.obtainForPriority(priority);
    }

    public ResponseEntity<String> eliminateForId(@PathVariable("id") Long id){
        boolean eliminate = serviceInterface.eliminateService(id);
        if (eliminate){
            return ResponseEntity.ok("Service eliminate " + id);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Failed delete service" + id);
        }
    }
}
