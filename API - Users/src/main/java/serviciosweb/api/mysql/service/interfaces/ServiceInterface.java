package serviciosweb.api.mysql.service.interfaces;

import org.springframework.stereotype.Service;
import serviciosweb.api.mysql.service.models.ServiceModel;

import java.util.ArrayList;
import java.util.Optional;

@Service
public interface ServiceInterface {
    ArrayList<ServiceModel> obtainService();
    ServiceModel saveService(ServiceModel service);
    Optional<ServiceModel> obtainForId(Long id);
    ArrayList<ServiceModel> obtainForPriority(Integer priority);
    boolean eliminateService(Long id);
}
