package serviciosweb.api.mysql.service.repositories;

import org.springframework.data.repository.CrudRepository;
import serviciosweb.api.mysql.service.models.ServiceModel;

import java.util.ArrayList;

public interface ServiceRepository extends CrudRepository<ServiceModel, Long> {
    ArrayList<ServiceModel> findByPriority(Integer priority);
}
