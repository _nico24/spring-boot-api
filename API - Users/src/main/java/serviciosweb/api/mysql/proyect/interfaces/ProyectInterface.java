package serviciosweb.api.mysql.proyect.interfaces;

import org.springframework.stereotype.Service;
import serviciosweb.api.mysql.proyect.models.ProyectModel;

import java.util.ArrayList;
import java.util.Optional;

@Service
public interface ProyectInterface {
    ArrayList<ProyectModel> obtainProyect();
    ProyectModel saveProyect(ProyectModel proyect);
    Optional<ProyectModel> obtainForId(Long id);
    ArrayList<ProyectModel> obtainForPriority(Integer priority);
    boolean eliminateProyect(Long id);

}
