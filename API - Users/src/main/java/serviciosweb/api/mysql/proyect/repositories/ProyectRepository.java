package serviciosweb.api.mysql.proyect.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import serviciosweb.api.mysql.proyect.models.ProyectModel;

import java.util.ArrayList;

@Repository
public interface ProyectRepository extends CrudRepository<ProyectModel, Long> {
    ArrayList<ProyectModel> findByPriority(Integer priority);
}
