package serviciosweb.api.mysql.proyect.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import serviciosweb.api.mysql.proyect.interfaces.ProyectInterface;
import serviciosweb.api.mysql.proyect.models.ProyectModel;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/proyect")
public class ProyectController {
    private final ProyectInterface proyectInterface;

    @Autowired
    public ProyectController(ProyectInterface proyectInterface) {
        this.proyectInterface = proyectInterface;
    }
    @GetMapping
    public List<ProyectModel> obtainProyect(){return  proyectInterface.obtainProyect();}

    @PostMapping
    public ProyectModel saveProyect(@RequestBody ProyectModel proyect){
        return proyectInterface.saveProyect(proyect);
    }

    @GetMapping(path = "/{id}")
    public Optional<ProyectModel> obtainProyectForId(@PathVariable("id") Long id){
        return proyectInterface.obtainForId(id);
    }

    @GetMapping("/{query}")
    public List<ProyectModel> obtainProyectForPriority(@RequestParam("priority") Integer priority){
        return proyectInterface.obtainForPriority(priority);
    }

    public ResponseEntity<String> eliminateForId(@PathVariable("id") Long id){
        boolean eliminate = proyectInterface.eliminateProyect(id);
        if (eliminate) {
            return ResponseEntity.ok("Proyect Delete " + id);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Failed Delete Proyect " + id);
        }
    }
}
