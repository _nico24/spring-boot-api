package serviciosweb.api.mysql.proyect.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import serviciosweb.api.mysql.proyect.interfaces.ProyectInterface;
import serviciosweb.api.mysql.proyect.models.ProyectModel;
import serviciosweb.api.mysql.proyect.repositories.ProyectRepository;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class ProyectService implements ProyectInterface {
    private final ProyectRepository proyectRepository;

    @Autowired
    public ProyectService(ProyectRepository proyectRepository) {
        this.proyectRepository = proyectRepository;
    }

    @Override
    public ArrayList<ProyectModel> obtainProyect() {
        return (ArrayList<ProyectModel>) proyectRepository.findAll();
    }

    @Override
    public ProyectModel saveProyect(ProyectModel proyect) {
        return proyectRepository.save(proyect);
    }

    @Override
    public Optional<ProyectModel> obtainForId(Long id) {
        return proyectRepository.findById(id);
    }

    @Override
    public ArrayList<ProyectModel> obtainForPriority(Integer priority) {
        return proyectRepository.findByPriority(priority);
    }

    @Override
    public boolean eliminateProyect(Long id) {
        try {
            proyectRepository.deleteById(id);
            return true;
        } catch (Exception err) {
            return false;
        }
    }
}
