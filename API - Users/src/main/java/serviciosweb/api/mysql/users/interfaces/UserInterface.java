package serviciosweb.api.mysql.users.interfaces;

import org.springframework.stereotype.Service;
import serviciosweb.api.mysql.users.models.UserModel;

import java.util.ArrayList;
import java.util.Optional;

@Service
public interface UserInterface {
    ArrayList<UserModel> obtainUser();
    UserModel saveUser(UserModel user);
    Optional<UserModel> obtainForId(Long id);
    ArrayList<UserModel> obtainForPriority(Integer priority);
    boolean eliminateUser(Long id);
}
