package serviciosweb.api.mysql.users.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import serviciosweb.api.mysql.users.interfaces.UserInterface;
import serviciosweb.api.mysql.users.models.UserModel;
import serviciosweb.api.mysql.users.repositories.UserRepository;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class UserService implements UserInterface {
    private final UserRepository userService;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userService = userRepository;
    }

    @Override
    public ArrayList<UserModel> obtainUser() {
        return (ArrayList<UserModel>) userService.findAll();
    }

    @Override
    public UserModel saveUser(UserModel user) {
        return userService.save(user);
    }

    @Override
    public Optional<UserModel> obtainForId(Long id) {
        return userService.findById(id);
    }

    @Override
    public ArrayList<UserModel> obtainForPriority(Integer priority) {
        return userService.findByPriority(priority);
    }

    @Override
    public boolean eliminateUser(Long id) {
        try {
            userService.deleteById(id);
            return true;
        } catch (Exception err) {
            return false;
        }
    }
}
