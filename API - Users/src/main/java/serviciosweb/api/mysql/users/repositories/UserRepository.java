package serviciosweb.api.mysql.users.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import serviciosweb.api.mysql.users.models.UserModel;

import java.util.ArrayList;

@Repository
public interface UserRepository extends CrudRepository<UserModel, Long> {
    ArrayList<UserModel> findByPriority(Integer priority);
}
