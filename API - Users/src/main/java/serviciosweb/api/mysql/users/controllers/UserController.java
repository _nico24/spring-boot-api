package serviciosweb.api.mysql.users.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import serviciosweb.api.mysql.users.models.UserModel;
import serviciosweb.api.mysql.users.interfaces.UserInterface;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/user")
public class UserController {
    private final UserInterface userInterface;

    @Autowired
    public UserController(UserInterface userService) {
        this.userInterface = userService;
    }

    @GetMapping()
    public List<UserModel> obtainUser() {
        return userInterface.obtainUser();
    }

    @PostMapping()
    public UserModel saveUser(@RequestBody UserModel user) {
        return userInterface.saveUser(user);
    }

    @GetMapping(path = "/{id}")
    public Optional<UserModel> obtainUserForId(@PathVariable("id") Long id) {
        return userInterface.obtainForId(id);
    }

    @GetMapping("/query")
    public List<UserModel> obtainUserForPriority(@RequestParam("priority") Integer priority) {
        return userInterface.obtainForPriority(priority);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> eliminateForId(@PathVariable("id") Long id) {
        boolean eliminate = userInterface.eliminateUser(id);
        if (eliminate) {
            return ResponseEntity.ok("User with ID was deleted " + id);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Could not find or delete user with ID " + id);
        }
    }
}
