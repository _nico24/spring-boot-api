package HonkaiStarRail.core.allcharacters.application.usecase;

import HonkaiStarRail.core.allcharacters.infrastructure.model.AllCharacters;
import HonkaiStarRail.core.allcharacters.ports.in.GetAllCharactersUseCase;
import HonkaiStarRail.core.allcharacters.repository.AllCharacterRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
@AllArgsConstructor
public class GetAllCharactersUseCaseImpl implements GetAllCharactersUseCase {

    private final AllCharacterRepository allCharacterRepository;

    @Override
    public ArrayList<AllCharacters> getAllCharacters() {
        return (ArrayList<AllCharacters>) allCharacterRepository.findAll();
    }
}
