package HonkaiStarRail.core.allcharacters.application.usecase;

import HonkaiStarRail.core.allcharacters.infrastructure.model.AllCharacters;
import HonkaiStarRail.core.allcharacters.ports.in.SaveCharactersUseCase;
import HonkaiStarRail.core.allcharacters.repository.AllCharacterRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class SaveCharactersUseCaseImpl implements SaveCharactersUseCase {

    private final AllCharacterRepository allCharacterRepository;

    @Override
    public AllCharacters saveCharacters(AllCharacters allCharacters) {
        return allCharacterRepository.save(allCharacters);
    }
}
