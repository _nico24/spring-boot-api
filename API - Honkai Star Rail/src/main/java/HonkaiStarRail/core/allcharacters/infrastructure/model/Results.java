package HonkaiStarRail.core.allcharacters.infrastructure.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;

@Entity
@Table(name = "results")
class Results {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String url;

    @ManyToOne
    @JoinColumn(name = "all_characters_id")
    @JsonBackReference
    private AllCharacters allCharacters;

    public Results() {
    }

    public Results(
            Long id,
            String name,
            String url,
            AllCharacters allCharacters) {
        this.id = id;
        this.name = name;
        this.url = url;
        this.allCharacters = allCharacters;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AllCharacters getAllCharacters() {
        return allCharacters;
    }

    public void setAllCharacters(AllCharacters allCharacters) {
        this.allCharacters = allCharacters;
    }
}
