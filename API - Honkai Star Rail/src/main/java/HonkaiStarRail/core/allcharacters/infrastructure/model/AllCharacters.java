package HonkaiStarRail.core.allcharacters.infrastructure.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name = "all_characters")
public class AllCharacters {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    @JsonIgnore
    private Long id;
    private int count;

    @OneToMany(mappedBy = "allCharacters")
    @JsonManagedReference
    private List<Results> result;

    public AllCharacters() {
    }

    public AllCharacters(
            int count,
            List<Results> result
    ) {
        this.count = count;
        this.result = result;
    }

    // Getters y setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<Results> getResult() {
        return result;
    }

    public void setResult(List<Results> result) {
        this.result = result;
    }
}
