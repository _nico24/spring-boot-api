package HonkaiStarRail.core.allcharacters.infrastructure.controller;

import HonkaiStarRail.core.allcharacters.infrastructure.model.AllCharacters;
import HonkaiStarRail.core.allcharacters.ports.in.GetAllCharactersUseCase;
import HonkaiStarRail.core.allcharacters.ports.in.SaveCharactersUseCase;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/home")
public class AllCharacterController {

    private final GetAllCharactersUseCase getAllCharacters;

    private final SaveCharactersUseCase saveCharacters;

    @GetMapping()
    public List<AllCharacters> obtainAllCharacters() {
        return getAllCharacters.getAllCharacters();
    }

    @PostMapping()
    public AllCharacters saveAllCharacters(@RequestBody AllCharacters allCharacters) {
        return saveCharacters.saveCharacters(allCharacters);
    }
}
