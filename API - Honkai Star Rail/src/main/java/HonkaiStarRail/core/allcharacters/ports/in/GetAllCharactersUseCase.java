package HonkaiStarRail.core.allcharacters.ports.in;


import HonkaiStarRail.core.allcharacters.infrastructure.model.AllCharacters;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public interface GetAllCharactersUseCase {

    ArrayList<AllCharacters> getAllCharacters();

}
