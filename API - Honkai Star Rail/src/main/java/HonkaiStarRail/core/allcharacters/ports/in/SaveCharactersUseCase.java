package HonkaiStarRail.core.allcharacters.ports.in;

import HonkaiStarRail.core.allcharacters.infrastructure.model.AllCharacters;
import org.springframework.stereotype.Service;

@Service
public interface SaveCharactersUseCase {


    AllCharacters saveCharacters(AllCharacters allCharacters);


}
