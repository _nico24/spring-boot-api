package HonkaiStarRail.core.allcharacters.repository;

import HonkaiStarRail.core.allcharacters.infrastructure.model.AllCharacters;
import org.springframework.data.repository.CrudRepository;

public interface AllCharacterRepository extends CrudRepository<AllCharacters, String> {
}