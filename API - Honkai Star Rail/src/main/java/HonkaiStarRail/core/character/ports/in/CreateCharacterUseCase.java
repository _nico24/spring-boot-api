package HonkaiStarRail.core.character.ports.in;

import HonkaiStarRail.core.character.infrastructure.model.Character;
import org.springframework.stereotype.Service;

@Service
public interface CreateCharacterUseCase {

    Character createCharacter(Character character);
}
