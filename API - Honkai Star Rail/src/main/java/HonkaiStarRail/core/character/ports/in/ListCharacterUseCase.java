package HonkaiStarRail.core.character.ports.in;

import HonkaiStarRail.core.character.infrastructure.model.Character;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public interface ListCharacterUseCase {

    ArrayList<Character> listCharacter();
}
