package HonkaiStarRail.core.character.ports.in;

import HonkaiStarRail.core.character.infrastructure.model.Character;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public interface ListForIdUseCase {

    Optional<Character> listForId(Long id);
}
