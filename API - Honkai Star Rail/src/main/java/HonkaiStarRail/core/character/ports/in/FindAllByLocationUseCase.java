package HonkaiStarRail.core.character.ports.in;

import HonkaiStarRail.core.character.infrastructure.model.Character;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface FindAllByLocationUseCase {

    List<Character> findAllByLocation(String location);
}
