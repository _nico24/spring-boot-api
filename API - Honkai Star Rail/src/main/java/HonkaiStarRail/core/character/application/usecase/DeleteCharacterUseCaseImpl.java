package HonkaiStarRail.core.character.application.usecase;

import HonkaiStarRail.core.character.ports.in.DeleteCharacterUseCase;
import HonkaiStarRail.core.character.repository.CharacterRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class DeleteCharacterUseCaseImpl implements DeleteCharacterUseCase {

    private final CharacterRepository characterRepository;

    @Override
    public boolean deleteCharacter(Long id) {
        try {
            characterRepository.deleteById(id);
            return true;
        } catch (Exception err) {
            return false;
        }
    }
}
