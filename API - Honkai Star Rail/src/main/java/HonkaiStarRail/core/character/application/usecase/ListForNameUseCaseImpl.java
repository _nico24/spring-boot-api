package HonkaiStarRail.core.character.application.usecase;

import HonkaiStarRail.core.character.infrastructure.model.Character;
import HonkaiStarRail.core.character.ports.in.ListForNameUseCase;
import HonkaiStarRail.core.character.repository.CharacterRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class ListForNameUseCaseImpl implements ListForNameUseCase {

    private final CharacterRepository characterRepository;

    @Override
    public Optional<Character> listForName(String name) {
        return characterRepository.findByName(name);
    }
}
