package HonkaiStarRail.core.character.application.usecase;

import HonkaiStarRail.core.character.infrastructure.model.Character;
import HonkaiStarRail.core.character.ports.in.ListCharacterUseCase;
import HonkaiStarRail.core.character.repository.CharacterRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
@AllArgsConstructor
public class ListCharacterUseCaseImpl implements ListCharacterUseCase {

    private final CharacterRepository characterRepository;

    @Override
    public ArrayList<Character> listCharacter() {
        return (ArrayList<Character>) characterRepository.findAll();
    }
}
