package HonkaiStarRail.core.character.application.usecase;

import HonkaiStarRail.core.character.infrastructure.model.Character;
import HonkaiStarRail.core.character.ports.in.ListForIdUseCase;
import HonkaiStarRail.core.character.repository.CharacterRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class ListForIdUseCaseImpl implements ListForIdUseCase {

    private final CharacterRepository characterRepository;

    @Override
    public Optional<Character> listForId(Long id) {
        return characterRepository.findById(id);
    }
}
