package HonkaiStarRail.core.character.application.usecase;

import HonkaiStarRail.core.character.infrastructure.model.Character;
import HonkaiStarRail.core.character.ports.in.CreateCharacterUseCase;
import HonkaiStarRail.core.character.repository.CharacterRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CreateCharacterUseCaseImpl implements CreateCharacterUseCase {

    private final CharacterRepository characterRepository;

    @Override
    public Character createCharacter(Character character) {
        return characterRepository.save(character);
    }
}
