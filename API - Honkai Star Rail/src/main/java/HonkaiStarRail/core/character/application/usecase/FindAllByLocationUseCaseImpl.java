package HonkaiStarRail.core.character.application.usecase;

import HonkaiStarRail.core.character.infrastructure.model.Character;
import HonkaiStarRail.core.character.ports.in.FindAllByLocationUseCase;
import HonkaiStarRail.core.character.repository.CharacterRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class FindAllByLocationUseCaseImpl implements FindAllByLocationUseCase {

    private final CharacterRepository characterRepository;

    @Override
    public List<Character> findAllByLocation(String location) {
        return characterRepository.findAllByLocation(location);
    }
}
