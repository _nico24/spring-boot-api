package HonkaiStarRail.core.character.infrastructure.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

@Entity
@Table(name = "sprites")
public class Sprites {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;
    private String splashscreen;
    private String card;
    @ManyToOne
    @JoinColumn(name = "character_id")
    @JsonBackReference
    private Character character;


    public Sprites() {
    }

    public Sprites(
            Long id,
            String splashscreen,
            String card,
            Character character
    ) {
        this.id = id;
        this.splashscreen = splashscreen;
        this.card = card;
        this.character = character;
    }

    public String getSplashscreen() {
        return splashscreen;
    }

    public void setSplashscreen(String splashscreen) {
        this.splashscreen = splashscreen;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Character getCharacter() {
        return character;
    }

    public void setCharacter(Character character) {
        this.character = character;
    }
}
