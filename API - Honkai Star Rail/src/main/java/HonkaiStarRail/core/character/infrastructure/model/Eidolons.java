package HonkaiStarRail.core.character.infrastructure.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

@Entity
@Table(name = "eidolons")
public class Eidolons {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;

    //Eidolon - 1
    @Lob
    @Column(columnDefinition = "LONGTEXT")
    private String eidolons1;

    @Lob
    @Column(columnDefinition = "LONGTEXT")
    private String description1;

    @Lob
    @Column(columnDefinition = "LONGTEXT")
    private String edilons1image;

    //Eidolon - 2
    @Lob
    @Column(columnDefinition = "LONGTEXT")
    private String eidolons2;

    @Lob
    @Column(columnDefinition = "LONGTEXT")
    private String description2;

    @Lob
    @Column(columnDefinition = "LONGTEXT")
    private String edilons2image;

    //Eidolon - 3
    @Lob
    @Column(columnDefinition = "LONGTEXT")
    private String eidolons3;

    @Lob
    @Column(columnDefinition = "LONGTEXT")
    private String description3;

    @Lob
    @Column(columnDefinition = "LONGTEXT")
    private String edilons3image;

    //Eidolon - 4
    @Lob
    @Column(columnDefinition = "LONGTEXT")
    private String eidolons4;

    @Lob
    @Column(columnDefinition = "LONGTEXT")
    private String description4;

    @Lob
    @Column(columnDefinition = "LONGTEXT")
    private String edilons4image;

    //Eidolon - 5
    @Lob
    @Column(columnDefinition = "LONGTEXT")
    private String eidolons5;

    @Lob
    @Column(columnDefinition = "LONGTEXT")
    private String description5;

    @Lob
    @Column(columnDefinition = "LONGTEXT")
    private String edilons5image;

    //Eidolon - 6
    @Lob
    @Column(columnDefinition = "LONGTEXT")
    private String eidolons6;

    @Lob
    @Column(columnDefinition = "LONGTEXT")
    private String description6;

    @Lob
    @Column(columnDefinition = "LONGTEXT")
    private String edilons6image;

    @ManyToOne
    @JoinColumn(name = "character_id")
    @JsonBackReference
    private Character character;

    public Eidolons() {
    }

    public Eidolons(
            Long id,
            String eidolons1,
            String description1,
            String edilons1image,
            String eidolons2,
            String description2,
            String edilons2image,
            String eidolons3,
            String description3,
            String edilons3image,
            String eidolons4,
            String description4,
            String edilons4image,
            String eidolons5,
            String description5,
            String edilons5image,
            String eidolons6,
            String description6,
            String edilons6image,
            Character character
    ) {
        this.id = id;
        this.eidolons1 = eidolons1;
        this.description1 = description1;
        this.edilons1image = edilons1image;
        this.eidolons2 = eidolons2;
        this.description2 = description2;
        this.edilons2image = edilons2image;
        this.eidolons3 = eidolons3;
        this.description3 = description3;
        this.edilons3image = edilons3image;
        this.eidolons4 = eidolons4;
        this.description4 = description4;
        this.edilons4image = edilons4image;
        this.eidolons5 = eidolons5;
        this.description5 = description5;
        this.edilons5image = edilons5image;
        this.eidolons6 = eidolons6;
        this.description6 = description6;
        this.edilons6image = edilons6image;
        this.character = character;
    }

    public String getEidolons1() {
        return eidolons1;
    }

    public void setEidolons1(String eidolons1) {
        this.eidolons1 = eidolons1;
    }

    public String getEdilons1image() {
        return edilons1image;
    }

    public void setEdilons1image(String edilons1image) {
        this.edilons1image = edilons1image;
    }

    public String getEidolons2() {
        return eidolons2;
    }

    public void setEidolons2(String eidolons2) {
        this.eidolons2 = eidolons2;
    }

    public String getEdilons2image() {
        return edilons2image;
    }

    public void setEdilons2image(String edilons2image) {
        this.edilons2image = edilons2image;
    }

    public String getEidolons3() {
        return eidolons3;
    }

    public void setEidolons3(String eidolons3) {
        this.eidolons3 = eidolons3;
    }

    public String getEdilons3image() {
        return edilons3image;
    }

    public void setEdilons3image(String edilons3image) {
        this.edilons3image = edilons3image;
    }

    public String getEidolons4() {
        return eidolons4;
    }

    public void setEidolons4(String eidolons4) {
        this.eidolons4 = eidolons4;
    }

    public String getEdilons4image() {
        return edilons4image;
    }

    public void setEdilons4image(String edilons4image) {
        this.edilons4image = edilons4image;
    }

    public String getEidolons5() {
        return eidolons5;
    }

    public void setEidolons5(String eidolons5) {
        this.eidolons5 = eidolons5;
    }

    public String getEdilons5image() {
        return edilons5image;
    }

    public void setEdilons5image(String edilons5image) {
        this.edilons5image = edilons5image;
    }

    public String getEidolons6() {
        return eidolons6;
    }

    public void setEidolons6(String eidolons6) {
        this.eidolons6 = eidolons6;
    }

    public String getEdilons6image() {
        return edilons6image;
    }

    public void setEdilons6image(String edilons6image) {
        this.edilons6image = edilons6image;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Character getCharacter() {
        return character;
    }

    public void setCharacter(Character character) {
        this.character = character;
    }

    public String getDescription1() {
        return description1;
    }

    public void setDescription1(String description1) {
        this.description1 = description1;
    }

    public String getDescription2() {
        return description2;
    }

    public void setDescription2(String description2) {
        this.description2 = description2;
    }

    public String getDescription3() {
        return description3;
    }

    public void setDescription3(String description3) {
        this.description3 = description3;
    }

    public String getDescription4() {
        return description4;
    }

    public void setDescription4(String description4) {
        this.description4 = description4;
    }

    public String getDescription5() {
        return description5;
    }

    public void setDescription5(String description5) {
        this.description5 = description5;
    }

    public String getDescription6() {
        return description6;
    }

    public void setDescription6(String description6) {
        this.description6 = description6;
    }
}
