package HonkaiStarRail.core.character.infrastructure.controller;

import HonkaiStarRail.core.character.ports.in.CreateCharacterUseCase;
import HonkaiStarRail.core.character.ports.in.DeleteCharacterUseCase;
import HonkaiStarRail.core.character.ports.in.FindAllByLocationUseCase;
import HonkaiStarRail.core.character.ports.in.ListCharacterUseCase;
import HonkaiStarRail.core.character.ports.in.ListForIdUseCase;
import HonkaiStarRail.core.character.ports.in.ListForNameUseCase;
import HonkaiStarRail.core.character.infrastructure.model.Character;

import lombok.AllArgsConstructor;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.DeleteMapping;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@RestController
@RequestMapping("/home/list")
public class CharacterController {

    private final CreateCharacterUseCase createCharacterUseCase;

    private final DeleteCharacterUseCase deleteCharacterUseCase;

    private final FindAllByLocationUseCase findAllByLocationUseCase;

    private final ListCharacterUseCase listCharacterUseCase;

    private final ListForIdUseCase listForIdUseCase;

    private final ListForNameUseCase listForNameUseCase;

    @GetMapping()
    public List<Character> listCharacter() {
        return listCharacterUseCase.listCharacter();
    }

    @PostMapping()
    public Character createCharacter(@RequestBody Character character) {
        return createCharacterUseCase.createCharacter(character);
    }

    @GetMapping(path = "/{id}")
    public Optional<Character> listForId(@PathVariable("id") Long id) {
        return listForIdUseCase.listForId(id);
    }

    @GetMapping(path = "/name/{name}")
    public Optional<Character> listForName(@PathVariable("name") String name) {
        return listForNameUseCase.listForName(name);
    }

    @GetMapping(path = "/location/{location}")
    public List<Character> findAllByLocation(@PathVariable("location") String location) {
        return findAllByLocationUseCase.findAllByLocation(location);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteCharacter(@PathVariable("id") Long id) {
        boolean eliminate = deleteCharacterUseCase.deleteCharacter(id);
        if (eliminate) {
            return ResponseEntity.ok("Character with ID was deleted" + id);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Could not find or delete character with ID" + id);
        }
    }

}
