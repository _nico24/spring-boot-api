package HonkaiStarRail.core.character.infrastructure.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

@Entity
@Table(name = "minstats")
public class MinStats {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;
    private String hp;
    private String atk;
    private String def;
    private String spd;
    private String crit_rate;
    private String crit_dmg;
    private String energy;
    private String taunt;

    @ManyToOne
    @JoinColumn(name = "list_stats_id")
    @JsonBackReference
    private ListStats listStats;

    public MinStats() {

    }

    public MinStats(
            Long id,
            String hp,
            String atk,
            String def,
            String spd,
            String critRate,
            String critDmg,
            String energy,
            String taunt,
            ListStats listStats
    ) {
        this.id = id;
        this.hp = hp;
        this.atk = atk;
        this.def = def;
        this.spd = spd;
        crit_rate = critRate;
        crit_dmg = critDmg;
        this.energy = energy;
        this.taunt = taunt;
        this.listStats = listStats;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHp() {
        return hp;
    }

    public void setHp(String hp) {
        this.hp = hp;
    }

    public String getAtk() {
        return atk;
    }

    public void setAtk(String atk) {
        this.atk = atk;
    }

    public String getDef() {
        return def;
    }

    public void setDef(String def) {
        this.def = def;
    }

    public String getSpd() {
        return spd;
    }

    public void setSpd(String spd) {
        this.spd = spd;
    }

    public String getTaunt() {
        return taunt;
    }

    public void setTaunt(String taunt) {
        this.taunt = taunt;
    }

    public ListStats getListStats() {
        return listStats;
    }

    public void setListStats(ListStats listStats) {
        this.listStats = listStats;
    }

    public String getCrit_rate() {
        return crit_rate;
    }

    public void setCrit_rate(String crit_rate) {
        this.crit_rate = crit_rate;
    }

    public String getCrit_dmg() {
        return crit_dmg;
    }

    public void setCrit_dmg(String crit_dmg) {
        this.crit_dmg = crit_dmg;
    }

    public String getEnergy() {
        return energy;
    }

    public void setEnergy(String energy) {
        this.energy = energy;
    }
}
