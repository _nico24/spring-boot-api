package HonkaiStarRail.core.character.infrastructure.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

@Entity
@Table(name = "abilities")
public class Abilities {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;

    @Lob
    @Column(columnDefinition = "LONGTEXT")
    private String atkBasicTitle;

    @Lob
    @Column(columnDefinition = "LONGTEXT")
    private String atkBasicDescription;

    @Lob
    @Column(columnDefinition = "LONGTEXT")
    private String abilityBasicTitle;

    @Lob
    @Column(columnDefinition = "LONGTEXT")
    private String abilityBasicDescription;

    @Lob
    @Column(columnDefinition = "LONGTEXT")
    private String ultimateAbilityTitle;

    @Lob
    @Column(columnDefinition = "LONGTEXT")
    private String ultimateAbilityDescription;

    @Lob
    @Column(columnDefinition = "LONGTEXT")
    private String talentTitle;

    @Lob
    @Column(columnDefinition = "LONGTEXT")
    private String talentDescription;

    @Lob
    @Column(columnDefinition = "LONGTEXT")
    private String techniqueTitle;

    @Lob
    @Column(columnDefinition = "LONGTEXT")
    private String techniqueDescription;

    @ManyToOne
    @JoinColumn(name = "character_id")
    @JsonBackReference
    private Character character;

    public Abilities() {
    }

    public Abilities(
            Long id, String atkBasicTitle,
            String atkBasicDescription,
            String abilityBasicTitle,
            String abilityBasicDescription,
            String ultimateAbilityTitle,
            String ultimateAbilityDescription,
            String talentTitle,
            String talentDescription,
            String techniqueTitle,
            String techniqueDescription,
            Character character
    ) {
        this.id = id;
        this.atkBasicTitle = atkBasicTitle;
        this.atkBasicDescription = atkBasicDescription;
        this.abilityBasicTitle = abilityBasicTitle;
        this.abilityBasicDescription = abilityBasicDescription;
        this.ultimateAbilityTitle = ultimateAbilityTitle;
        this.ultimateAbilityDescription = ultimateAbilityDescription;
        this.talentTitle = talentTitle;
        this.talentDescription = talentDescription;
        this.techniqueTitle = techniqueTitle;
        this.techniqueDescription = techniqueDescription;
        this.character = character;
    }

    public String getAtkBasicTitle() {
        return atkBasicTitle;
    }

    public void setAtkBasicTitle(String atkBasicTitle) {
        this.atkBasicTitle = atkBasicTitle;
    }

    public String getAtkBasicDescription() {
        return atkBasicDescription;
    }

    public void setAtkBasicDescription(String atkBasicDescription) {
        this.atkBasicDescription = atkBasicDescription;
    }

    public String getAbilityBasicTitle() {
        return abilityBasicTitle;
    }

    public void setAbilityBasicTitle(String abilityBasicTitle) {
        this.abilityBasicTitle = abilityBasicTitle;
    }

    public String getAbilityBasicDescription() {
        return abilityBasicDescription;
    }

    public void setAbilityBasicDescription(String abilityBasicDescription) {
        this.abilityBasicDescription = abilityBasicDescription;
    }

    public String getUltimateAbilityTitle() {
        return ultimateAbilityTitle;
    }

    public void setUltimateAbilityTitle(String ultimateAbilityTitle) {
        this.ultimateAbilityTitle = ultimateAbilityTitle;
    }

    public String getUltimateAbilityDescription() {
        return ultimateAbilityDescription;
    }

    public void setUltimateAbilityDescription(String ultimateAbilityDescription) {
        this.ultimateAbilityDescription = ultimateAbilityDescription;
    }

    public String getTalentTitle() {
        return talentTitle;
    }

    public void setTalentTitle(String talentTitle) {
        this.talentTitle = talentTitle;
    }

    public String getTalentDescription() {
        return talentDescription;
    }

    public void setTalentDescription(String talentDescription) {
        this.talentDescription = talentDescription;
    }

    public String getTechniqueTitle() {
        return techniqueTitle;
    }

    public void setTechniqueTitle(String techniqueTitle) {
        this.techniqueTitle = techniqueTitle;
    }

    public String getTechniqueDescription() {
        return techniqueDescription;
    }

    public void setTechniqueDescription(String techniqueDescription) {
        this.techniqueDescription = techniqueDescription;
    }

    public Character getCharacter() {
        return character;
    }

    public void setCharacter(Character character) {
        this.character = character;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
