package HonkaiStarRail.core.character.infrastructure.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

@Entity
@Table(name = "profile")
public class Profile {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;

    @Lob
    @Column(columnDefinition = "LONGTEXT")
    private String profile;

    @ManyToOne
    @JoinColumn(name = "character_id")
    @JsonBackReference
    private Character character;

    public Profile() {

    }

    public Profile(
            Long id,
            String profile,
            Character character
    ) {
        this.id = id;
        this.profile = profile;
        this.character = character;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public Character getCharacter() {
        return character;
    }

    public void setCharacter(Character character) {
        this.character = character;
    }
}
