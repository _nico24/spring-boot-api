package HonkaiStarRail.core.character.infrastructure.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

@Entity
@Table(name = "dialogues")
public class Dialogues {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;

    @Lob
    @Column(columnDefinition = "LONGTEXT")
    private String first_meeting;

    @Lob
    @Column(columnDefinition = "LONGTEXT")
    private String greeting;

    @Lob
    @Column(columnDefinition = "LONGTEXT")
    private String farewell;

    @Lob
    @Column(columnDefinition = "LONGTEXT")
    private String Conversation;

    @Lob
    @Column(columnDefinition = "LONGTEXT")
    private String about_self;

    @Lob
    @Column(columnDefinition = "LONGTEXT")
    private String hobbies;

    @Lob
    @Column(columnDefinition = "LONGTEXT")
    private String inconvenience;

    @Lob
    @Column(columnDefinition = "LONGTEXT")
    private String something_to_share;

    @Lob
    @Column(columnDefinition = "LONGTEXT")
    private String a_data;

    @ManyToOne
    @JoinColumn(name = "character_id")
    @JsonBackReference
    private Character character;

    public Dialogues() {

    }

    public Dialogues(
            Long id,
            String first_meeting,
            String greeting,
            String farewell,
            String conversation,
            String aboutSelf,
            String hobbies,
            String inconvenience,
            String something_to_share,
            String a_data
    ) {
        this.id = id;
        this.first_meeting = first_meeting;
        this.greeting = greeting;
        this.farewell = farewell;
        Conversation = conversation;
        about_self = aboutSelf;

        this.hobbies = hobbies;
        this.inconvenience = inconvenience;
        this.something_to_share = something_to_share;
        this.a_data = a_data;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirst_meeting() {
        return first_meeting;
    }

    public void setFirst_meeting(String first_meeting) {
        this.first_meeting = first_meeting;
    }

    public String getGreeting() {
        return greeting;
    }

    public void setGreeting(String greeting) {
        this.greeting = greeting;
    }

    public String getFarewell() {
        return farewell;
    }

    public void setFarewell(String farewell) {
        this.farewell = farewell;
    }

    public String getConversation() {
        return Conversation;
    }

    public void setConversation(String conversation) {
        Conversation = conversation;
    }

    public String getHobbies() {
        return hobbies;
    }

    public void setHobbies(String hobbies) {
        this.hobbies = hobbies;
    }

    public String getInconvenience() {
        return inconvenience;
    }

    public void setInconvenience(String inconvenience) {
        this.inconvenience = inconvenience;
    }

    public String getSomething_to_share() {
        return something_to_share;
    }

    public void setSomething_to_share(String something_to_share) {
        this.something_to_share = something_to_share;
    }

    public String getA_data() {
        return a_data;
    }

    public void setA_data(String a_data) {
        this.a_data = a_data;
    }

    public String getAbout_self() {
        return about_self;
    }

    public void setAbout_self(String about_self) {
        this.about_self = about_self;
    }
}
