package HonkaiStarRail.core.character.infrastructure.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name = "characters")
public class Character {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long id;
    private String name;

    @OneToMany(mappedBy = "character")
    @JsonManagedReference
    private List<About> about;

    @OneToMany(mappedBy = "character")
    @JsonManagedReference
    private List<Profile> profile;

    @OneToMany(mappedBy = "character")
    @JsonManagedReference
    private List<Appearance> appearance;

    @OneToMany(mappedBy = "character")
    @JsonManagedReference
    private List<ListStats> stats;
    private String element;
    private String path;
    private String category;
    private String rarity;
    private String gender;
    private String voice;
    private String location;


    @OneToMany(mappedBy = "character")
    @JsonManagedReference
    @Lob
    private List<Story> story;

    @OneToMany(mappedBy = "character")
    @JsonManagedReference
    private List<Dialogues> dialogues;

    @OneToMany(mappedBy = "character")
    @JsonManagedReference
    private List<Abilities> abilities;

    @OneToMany(mappedBy = "character")
    @JsonManagedReference
    private List<Eidolons> eidolons;

    @OneToMany(mappedBy = "character")
    @JsonManagedReference
    private List<Sprites> sprites;


    public Character() {
    }

    public Character(
            Long id,
            String name,
            List<About> about2,
            List<Profile> profile1,
            List<Appearance> appearance1,
            List<ListStats> stats,
            String element,
            String path,
            String category,
            String rarity,
            String gender,
            String voice,
            String location,
            List<Story> story,
            List<Dialogues> dialogues,
            List<Abilities> abilities,
            List<Eidolons> eidolons,
            List<Sprites> sprites
    ) {
        this.id = id;
        this.name = name;
        this.about = about2;
        this.profile = profile1;
        this.appearance = appearance1;
        this.stats = stats;
        this.element = element;
        this.path = path;
        this.category = category;
        this.rarity = rarity;
        this.gender = gender;
        this.voice = voice;
        this.location = location;
        this.story = story;
        this.dialogues = dialogues;
        this.abilities = abilities;
        this.eidolons = eidolons;
        this.sprites = sprites;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVoice() {
        return voice;
    }

    public void setVoice(String voice) {
        this.voice = voice;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public List<Story> getStory() {
        return story;
    }

    public void setStory(List<Story> story) {
        this.story = story;
    }

    public List<Abilities> getAbilities() {
        return abilities;
    }

    public void setAbilities(List<Abilities> abilities) {
        this.abilities = abilities;
    }

    public List<Eidolons> getEidolons() {
        return eidolons;
    }

    public void setEidolons(List<Eidolons> eidolons) {
        this.eidolons = eidolons;
    }

    public List<Sprites> getSprites() {
        return sprites;
    }

    public void setSprites(List<Sprites> sprites) {
        this.sprites = sprites;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public List<ListStats> getStats() {
        return stats;
    }

    public void setStats(List<ListStats> stats) {
        this.stats = stats;
    }

    public String getElement() {
        return element;
    }

    public void setElement(String element) {
        this.element = element;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getRarity() {
        return rarity;
    }

    public void setRarity(String rarity) {
        this.rarity = rarity;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<Dialogues> getDialogues() {
        return dialogues;
    }

    public void setDialogues(List<Dialogues> dialogues) {
        this.dialogues = dialogues;
    }


    public List<About> getAbout() {
        return about;
    }

    public void setAbout(List<About> about) {
        this.about = about;
    }

    public List<Profile> getProfile() {
        return profile;
    }

    public void setProfile(List<Profile> profile) {
        this.profile = profile;
    }

    public List<Appearance> getAppearance() {
        return appearance;
    }

    public void setAppearance(List<Appearance> appearance) {
        this.appearance = appearance;
    }
}
