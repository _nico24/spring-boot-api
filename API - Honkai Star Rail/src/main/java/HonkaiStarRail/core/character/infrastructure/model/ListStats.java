package HonkaiStarRail.core.character.infrastructure.model;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name = "liststats")
public class ListStats {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;

    @OneToMany(mappedBy = "listStats")
    private List<MinStats> minStats;

    @OneToMany(mappedBy = "listStats")
    private List<MaxStats> maxStats;

    @ManyToOne
    @JoinColumn(name = "character_id")
    @JsonBackReference
    private Character character;

    public ListStats() {

    }

    public ListStats(
            Long id,
            List<MinStats> minStats,
            List<MaxStats> maxStats
    ) {
        this.id = id;
        this.minStats = minStats;
        this.maxStats = maxStats;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<MinStats> getMinStats() {
        return minStats;
    }

    public void setMinStats(List<MinStats> minStats) {
        this.minStats = minStats;
    }

    public List<MaxStats> getMaxStats() {
        return maxStats;
    }

    public void setMaxStats(List<MaxStats> maxStats) {
        this.maxStats = maxStats;
    }
}
