package HonkaiStarRail.core.character.infrastructure.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

@Entity
@Table(name = "story")
public class Story {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;

    @Lob
    @Column(columnDefinition = "LONGTEXT")
    private String story1;

    @Lob
    @Column(columnDefinition = "LONGTEXT")
    private String story2;

    @Lob
    @Column(columnDefinition = "LONGTEXT")
    private String story3;

    @Lob
    @Column(columnDefinition = "LONGTEXT")
    private String story4;

    @ManyToOne
    @JoinColumn(name = "character_id")
    @JsonBackReference
    private Character character;

    public Story() {
    }

    public Story(
            Long id,
            String story1,
            String story2,
            String story3,
            String story4,
            Character character
    ) {
        this.id = id;
        this.story1 = story1;
        this.story2 = story2;
        this.story3 = story3;
        this.story4 = story4;
        this.character = character;
    }

    public String getStory1() {
        return story1;
    }

    public void setStory1(String story1) {
        this.story1 = story1;
    }

    public String getStory2() {
        return story2;
    }

    public void setStory2(String story2) {
        this.story2 = story2;
    }

    public String getStory3() {
        return story3;
    }

    public void setStory3(String story3) {
        this.story3 = story3;
    }

    public String getStory4() {
        return story4;
    }

    public void setStory4(String story4) {
        this.story4 = story4;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Character getCharacter() {
        return character;
    }

    public void setCharacter(Character character) {
        this.character = character;
    }
}
