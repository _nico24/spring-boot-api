package HonkaiStarRail.core.character.repository;

import HonkaiStarRail.core.character.infrastructure.model.Character;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


import java.util.List;
import java.util.Optional;

@Repository
public interface CharacterRepository extends CrudRepository<Character, Long> {
    List<Character> findAllByLocation(String location);

    Optional<Character> findByName(String name);
}