package HonkaiStarRail.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiStarRailApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiStarRailApplication.class, args);
    }

}
