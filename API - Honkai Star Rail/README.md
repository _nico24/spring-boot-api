# API Local de Personajes de Honkai Star Rail

Esta API local de personajes de Honkai Star Rail es una aplicación que te permite gestionar la información de los
personajes del juego, incluyendo detalles como su nombre, atributos, historia, diálogos, habilidades, entre otros.
Puedes utilizar esta API para agregar, buscar y eliminar personajes del juego. [Desarrollada por mi]()!

### Te dejo la Base de datos dentro del proyecto para que puedas hacer tus consultas!

## Requisitos Previos

Asegúrate de tener instalados los siguientes requisitos previos antes de utilizar esta API:

- [ ] MySQL Server: Debes tener un servidor MySQL configurado y en ejecución en tu máquina local o en un servidor
  remoto.
- [ ] Postman: Para probar las solicitudes HTTP a la API.
- [ ] Clonar el repositorio: Asegúrate de clonar este repositorio para acceder al código fuente y la base de datos
  exportada.

## Uso
### Listar todos los personajes
Para obtener una lista de todos los personajes, realiza una solicitud GET a la siguiente URL:
```
http://localhost:8080/home
```

### Listar la descripción de todos los personajes

Para obtener una descripción detallada de todos los personajes, realiza una solicitud GET a la siguiente URL:

```
http://localhost:8080/home/list
```

### Crear un nuevo personaje

Para agregar un nuevo personaje, realiza una solicitud POST a la misma URL mencionada anteriormente. Asegúrate de enviar
los datos del personaje en el cuerpo de la solicitud en formato JSON. Por ejemplo:

```json
{
  "name": "Nombre del personaje",
  "element": "Elemento del personaje",
  "rarity": "Rareza del personaje",
  "location": "Ubicación del personaje"
}
```
Esto es un ejemplo recuerda agregar los otros atributos del personaje!.
## Consultas

### Buscar un personaje por ID

Para buscar un personaje por su ID, realiza una solicitud GET a la siguiente URL, especificando el ID del personaje:

```
http://localhost:8080/home/list/{id}
```

### Buscar un personaje por nombre

Para buscar un personaje por su nombre, realiza una solicitud GET a la siguiente URL, especificando el nombre del
personaje:

```
http://localhost:8080/home/list/name/{nombre}
```

### Buscar personajes por ubicación

Para buscar todos los personajes en una ubicación específica, realiza una solicitud GET a la siguiente URL,
especificando la ubicación:

```
http://localhost:8080/home/list/location/{ubicacion}
```

### Eliminar un personaje

Para eliminar un personaje existente, realiza una solicitud DELETE a la siguiente URL, especificando el ID del personaje
que deseas eliminar:
```
http://localhost:8080/home/list/{id}
```

### Agradecimientos

¡Gracias por tu interés en la API de personajes de Honkai Star Rail! Si tienes alguna pregunta o sugerencia, no dudes en
ponerte en contacto conmigo.

