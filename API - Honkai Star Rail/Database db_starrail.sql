-- MySQL dump 10.13  Distrib 8.0.36, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: db_starrail
-- ------------------------------------------------------
-- Server version	8.0.36-0ubuntu0.22.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `abilities`
--

DROP TABLE IF EXISTS `abilities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `abilities` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `ability_basic_description` longtext,
  `ability_basic_title` longtext,
  `atk_basic_description` longtext,
  `atk_basic_title` longtext,
  `talent_description` longtext,
  `talent_title` longtext,
  `technique_description` longtext,
  `technique_title` longtext,
  `ultimate_ability_description` longtext,
  `ultimate_ability_title` longtext,
  `character_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKly47fobtd9rrhs14xi8i5208j` (`character_id`),
  CONSTRAINT `FKly47fobtd9rrhs14xi8i5208j` FOREIGN KEY (`character_id`) REFERENCES `characters` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `abilities`
--

LOCK TABLES `abilities` WRITE;
/*!40000 ALTER TABLE `abilities` DISABLE KEYS */;
/*!40000 ALTER TABLE `abilities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `about`
--

DROP TABLE IF EXISTS `about`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `about` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `about` longtext,
  `character_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK8hl242n92pd01rxbeya02gt4i` (`character_id`),
  CONSTRAINT `FK8hl242n92pd01rxbeya02gt4i` FOREIGN KEY (`character_id`) REFERENCES `characters` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `about`
--

LOCK TABLES `about` WRITE;
/*!40000 ALTER TABLE `about` DISABLE KEYS */;
INSERT INTO `about` VALUES (1,'El jefe del Departamento de Seguridad de la Estación Espacial Herta. No es muy hablador.\nArlan no entiende de investigaciones científicas, pero está dispuesto a arriesgar su vida para proteger a los investigadores y que puedan completar su trabajo. Está acostumbrado al dolor y luce sus cicatrices como si fueran medallas.\nSolo baja la guardia y muestra una sonrisa cuando juega con Peppy.',1),(2,'Una joven muy curiosa y energética que también es la investigadora jefa de la Estación Espacial Herta.\nAsta lo hace todo sin esfuerzo, ya se trate de lidiar con empleados obstinados o de responder a la Sociedad del Conocimiento con educación y firmeza.\nDespués de todo, ¡liderar una Estación Espacial es mucho más fácil que heredar una empresa familiar!',2),(3,'Una joven vivaz de la raza vidyadhara. Es conocida como la \"Dama Sanadora\" debido a sus vastos conocimientos de medicina.\nSuele recetar medidas poco ortodoxas como \"mantente bien hidratado\" y \"descansa mejor por la noche\".\nBailu no soporta ver sufrir a la gente, y por eso mantiene los ojos cerrados mientras cura dolencias.\n\"Lo único que importa es que se curen, ¿no?\".',3),(4,'Un espadachín que abandonó su cuerpo para convertirse en una espada. Se desconoce su nombre de nacimiento.\nJuró lealtad al Esclavo del Destino y posee una terrorífica habilidad de autocuración.\nBlade empuña una espada antigua llena de grietas, al igual que su cuerpo y su mente.',4),(5,'La heredera de la Guardiana Suprema de Belobog. También es la joven pero competente comandante de la Guardia Crinargenta.\nDesde pequeña, Bronya recibió una educación muy estricta y posee la conducta y la afinidad que se esperan de una heredera.\nPero, tras presenciar la situación lamentable del Bajomundo, la semilla de la duda comenzó a brotar en la mente de la futura gobernante de Belobog. \"¿La formación que recibí realmente puede ayudar a la gente a conseguir la vida que quiere?\".',5),(6,'Una niña criada por un robot. Su sensibilidad y su tenacidad son mayores de lo que cabría esperar por su edad.\nPara Clara, los cálculos lógicos de Svarog son infalibles.\nHasta que se dio cuenta de que los resultados obtenidos tras esos cálculos no tienen por qué hacer feliz a la gente.\nEn ese momento, la niña tímida decidió armarse de valor.',6),(7,'Un joven distante y reservado que blande una lanza llamada Perforanubes. Es el guardia del Expreso durante su larga expedición trazacaminos.\nDan Heng no habla mucho de su pasado. De hecho, se unió a la tripulación para escapar de él.\nPero ¿podrá el tren alejarlo de su pasado?',7),(8,'La forma original Vidyadhara de Dan Heng contiene el poder que queda de su vida anterior como Imbibitor Lunae. Al aceptar la majestuosa corona de astas, también tiene que adoptar todos los méritos y las faltas atribuídos a ese pecador.',8),(9,'La Gran Adivina de la Comisión de Adivinación del Luofu de Xianzhou. Una sabia que rebosa confianza y franqueza.\n\nEs quien utiliza el tercer ojo y la Matriz del Presagio para predecir la ruta de Xianzhou y los resultados de los acontecimientos. Cree tajantemente que todo lo que hace es la \"mejor solución\" ante cualquier problema.\n\nFu Xuan no pierde la esperanza de que el general cumpla su promesa de abdicar. Sin embargo, ese día parece cada vez más lejano.',9),(10,'El honorable y honrado capitán de la Guardia Crinargenta, descendiente de la aristocrática familia Landau.\nEn Belobog, la ciudad azotada por el frío, la vida puede continuar con normalidad...\nY en gran parte es gracias a que Gepard y sus Guardias Crinargenta garantizan la paz.',10),(11,'La verdadera dueña de la Estación Espacial Herta.\nEs la humana con mayor cociente intelectual de todo el Azul y solo se dedica a las cosas que le interesan. Cuando pierde el interés, el proyecto en el que estuviera inmersa cae inmediatamente en el olvido. La Estación Espacial es un buen ejemplo de ello.\nNormalmente se muestra en forma de marioneta que, según Herta, se parece bastante a ella cuando era niña.',11),(12,'Una científica aventurera que encontró el Expreso Astral varado en su planeta cuando era niña.\nVarios años después, cuando Himeko al fin logró reparar el Expreso Astral y comenzó su viaje por el espacio, se dio cuenta de que ese no era más que el comienzo. En su nueva trayectoria como trazacaminos, iba a necesitar muchos compañeros...\nY aunque todos tengan destinos diferentes, todos observan el mismo cielo estrellado.',12),(13,'La jefa del grupo de aventureros \"Los Topos\". Dice llamarse Gran Hook de la Oscuridad.\nNo le gusta que la llamen \"niña\" y piensa que puede encargarse de todo ella sola, sin la ayuda de ningún adulto.\nLos adultos se aventuran en el Fragmentum, Sampo se aventura a la superficie y los pacientes van con Natasha para curarse las heridas que sufren durante sus aventuras... ¡Guiados por Hook, los niños también pueden vivir sus propias aventuras!',13),(15,'Kafka aparece en la lista de más buscados de la Corporación para la Paz Interastral, pero su entrada solo contiene dos datos: su nombre y la frase \"Le gusta coleccionar abrigos\". Apenas se sabe nada sobre esta Cazadora de Estelaron, aparte de que es una de las colaboradoras más cercanas de Elio, el Esclavo del Destino.\nPara alcanzar el futuro previsto por Elio, Kafka se pone en marcha.',15),(16,'Un luchador con un brazo robótico, optimista y alegre. Miembro de Llamarada.\n\nEn el ring y en el campo de batalla, como boxeador y como luchador, Luka utiliza su poder para proteger a la gente del Bajomundo.\n\nÉl sabe lo que es perder la esperanza, así que está dispuesto a ayudar a los demás a recuperar la suya.',16),(17,'Un apuesto joven rubio que lleva un ataúd gigante a la espalda.\nComo comerciante intergaláctico, desgraciadamente se vio atrapado en la crisis del Estelaron en el Luofu de Xianzhou.\nY así fue como descubrió que su dominio de la medicina le resultaba útil.',17),(18,'La más joven de la familia Landau, la mejor exploradora de Belobog.\n\nPuede parecer indolente, pero en realidad es muy capaz. Muestra una actitud hermética solo para evitar socializar innecesariamente.\n\nEn cuanto a cómo define la socialización innecesaria: \"Humm... ¿No son innecesarias todas las interacciones sociales?\".',18),(19,'Una doctora muy meticulosa que siempre luce una curiosa sonrisa.\nLos recursos médicos escasean en el Bajomundo, y Natasha es una de las pocas doctoras a la que pueden acudir sus habitantes.\nIncluso la deslenguada Hook se comporta como una niña buena en su presencia.',19),(20,'La meticulosa oficial de inteligencia de la Guardia Crinargenta. Es bastante joven, pero muy inteligente.\nPela responde a todas las cuestiones con una certeza calmada, ya estén relacionadas con las maniobras de las tropas, la distribución de los suministros o las condiciones del terreno.\nEn cuanto a la funda de su teléfono... \"Eso no tiene nada que ver con el trabajo, capitán\".',20),(21,'Una adivinadora ordinaria de la Comisión de Adivinación que nunca flojea cuando se trata de holgazanear.\nAprobó el examen de acceso a la Comisión de Adivinación para complacer a sus padres, pero descubrió que lo que pensaba que era un trabajo relajado, en realidad era muy duro e intenso.\nTras varios años de experiencia, Qingque por fin ha perfeccionado sus habilidades. Pero no importa a qué departamento se cambie, sigue siendo la adivina de menor rango.\n\"¿Qué más podría desear en la vida que enterrar la cabeza en libros y jugar antiguos juegos de fichas?\".',21),(22,'Un hombre de negocios con mucha labia y que siempre estará en cualquier lugar en el que se pueda sacar tajada.\nLa información que posee hace que no quede más remedio que contar con él, pero convertirse en su cliente no siempre es algo bueno.\nAl fin y al cabo, un cliente puede convertirse en un producto si el precio es el adecuado.',22),(23,'Una valiente miembro de Llamarada que creció en el entorno hostil del Bajomundo de Belobog. Está acostumbrada a estar sola.\nLos protectores y los protegidos, los opresores y los oprimidos... El mundo en el que Seele creció era una dicotomía muy sencilla...\nHasta que apareció \"esa chica\".',23),(24,'La rebelde hija mayor de la familia Landau. Antes era la mejor amiga de Cocolia, y ahora lo que le interesa es la mecánica.\nEn el frío perpetuo de Belobog, Serval abrió un taller llamado Nuncainvierno que a menudo cierra para ofrecer conciertos de rock.\nSi alguien preguntara si el taller resulta rentable, Serval contestaría: \"Esto no es más que una afición. A mí me sobra el dinero\".',24),(25,'Una chica vivaz y excéntrica apasionada por todo lo que cree que interesa a las muchachas de su edad. Por ejemplo, tomar fotos...\nDespertó tras pasar una eternidad dando vueltas por el espacio dentro de un bloque de hielo y desconoce todo lo relativo a su identidad y su pasado. Tras un breve periodo de desánimo, adoptó como nombre la fecha en la que renació.\nEse día, Siete de Marzo comenzó su nueva vida.',25),(26,'El universo no es más que otro juego para esta superhacker.\nPor muy espinoso que sea el sistema de defensa, Silver Wolf puede descifrarlo con facilidad. Su batalla informática contra Tornillum, del Círculo de Genios, se ha convertido en una leyenda en la comunidad hacker.\n¿Cuántos niveles más hay que superar en este universo? Silver Wolf está deseando averiguarlo.',26),(27,'Una ingenua y entusiasta novata de los Nimbocaballeros que empuña una pesada espada.\nAnhela las leyendas históricas de los Nimbocaballeros y está ansiosa por convertirse ella misma en una figura similar.\nPor ello, Sushang sigue firmemente las filosofías de \"ayudar a los necesitados\", \"hacer una buena acción al día\" y \"reflexionar sobre una misma tres veces al día\", lo que la lleva a tener unos días muy ocupados asistiendo a los demás.',27),(28,'Tingyun es una raposiana elocuente y ostenta el cargo de aeroembajadora de Llamas Silbantes, el gremio de comerciantes de la Comisión del Transporte Celeste.\nSu facilidad de palabra hace que su público espere ansiosamente más historias cautivadoras. Gracias a su supervisión, las ferias comerciales de Xianzhou se volvieron conocidas en toda la galaxia.\n\"Intentar evitar el conflicto siempre que sea posible, y persuadir a los que pueden ser persuadidos\", ese es el lema de Tingyun.',28),(29,'Al principio del juego, Kafka con la ayuda de Silver Wolf activó su receptáculo, despertándolos al mundo real donde despertaron dentro de la Estación Espacial Herta durante la invasión de la Legión Antimateria.',29),(30,'Al principio del juego, Kafka con la ayuda de Silver Wolf activó su receptáculo, despertándolos al mundo real donde despertaron dentro de la Estación Espacial Herta durante la invasión de la Legión Antimateria.',30),(31,'Al principio del juego, Kafka con la ayuda de Silver Wolf activó su receptáculo, despertándolos al mundo real donde despertaron dentro de la Estación Espacial Herta durante la invasión de la Legión Antimateria.',31),(32,'Al principio del juego, Kafka con la ayuda de Silver Wolf activó su receptáculo, despertándolos al mundo real donde despertaron dentro de la Estación Espacial Herta durante la invasión de la Legión Antimateria.',32),(33,'Welt, el antiguo Soberano Antientropía, sabio y experimentado, heredó el nombre del mundo y lo salvó muchas veces de la aniquilación.\nCuando finalizó el incidente de St. Fontaine, Welt no tuvo más remedio que cruzar el portal con quien provocó ese incidente.\nProbablemente, ni él mismo sabía que lo que le esperaba al otro lado era un nuevo viaje y nuevos compañeros.',33),(34,'Un teniente de los Nimbocaballeros muy animoso, y también el espadachín más hábil del Luofu de Xianzhou.\nHa nacido para las espadas y está obsesionado con ellas. Siempre que una espada descansa en su mano, nadie se atreve a subestimar a este talento, aunque aún sea un niño.\nQuizá lo único capaz de opacar el filo de su preciada espada sea el tiempo.\n\n',34),(35,'La jefa de la Comisión del Transporte Celeste del Luofu de Xianzhou, aunque experimentada y autoritaria, es amable.\nComo piloto veterana desde una temprana edad, se convirtió en la líder de la Comisión gracias a sus extraordinarios logros, aunque ya no vuela debido a una batalla que fue especialmente despiadada.\nSu brillo se ha desvanecido ahora que ha pasado a centrarse en tareas de gestión, pero siempre está guiando la trayectoria del Luofu.',35),(36,'Es la personificación de la justicia y el honor, además de poseer una nobleza admirable. Como un vagabundo solitario en el cosmos, abraza de todo corazón los principios de la Belleza.\nMantener el honor de la belleza en el universo es el honor sagrado de Argenti. Desde el inicio, ha desempeñado su deber con una convicción piadosa e inquebrantable.',36),(37,'Una Guardamemorias del Jardín de los Recuerdos. Una adivina misteriosa y elegante. A menudo luce una amable sonrisa y está dispuesta a escuchar pacientemente lo que le cuenten, lo que usa como pretexto para introducirse en los recuerdos y averiguarlo todo sobre ciertos asuntos. Una dama a la que le apasiona recopilar recuerdos únicos, aunque es difícil deducir cuáles son sus intenciones.',37),(38,'Convencido de que la inteligencia y la creatividad no son exclusivas de los genios, se dedica a difundir el conocimiento en todo el cosmos para curar la persistente enfermedad llamada estupidez.',38),(39,'Se enfrenta a una nueva vida en el Luofu. Con su adoración por la cultura de Xianzhou como apoyo, Guinaifen aprendió rápidamente habilidades para mantenerse abrigada y alimentada, como comer fideos boca abajo, romper losas de piedra sobre el pecho de otros, atrapar proyectiles con las manos desnudas, etc.',39),(40,'Tras pasar tanto tiempo usando la oniromancia para predecir delitos kármicos y sufrir el desgaste causado por la ingente cantidad de información de los poseídos por Mara, se ha vuelto insensible a todo lo demás. Solo cuando actúa junto a su compañera jueza y hermana, Xueyi, Hanya revela por un momento su verdadera naturaleza.',40),(41,'Teme a los demonios, pero siempre se le encomienda la ardua tarea de capturar y erradicar a los espíritus malignos. Aunque se cree incapaz, no logra reunir el valor necesario para renunciar y sigue adelante tragándose su propio miedo.',41),(42,'Una de los héroes legendarios que formaban el Quinteto Nube Alta y a la que se otorgó el título de \"Destello Trascendental\".\n\nAl trascender los conceptos mortales de victoria, eligió recorrer un camino diferente para obtener el poder de matar dioses. A partir de entonces, se eliminó a una maestra de espadas de los registros del Luofu de Xianzhou y se añadió a una traidora más cuyo nombre ha sido tachado.',42),(43,'Es extremadamente trabajador y muy hábil reparando todo tipo de máquinas. También le gusta escuchar los rumores interestelares que le cuentan los huéspedes. Quiere crecer rápido para poder embarcarse en un viaje y recorrer las estrellas.',43),(44,'Se ganó la atención de Nous gracias a su talento y asombrosa dedicación. Comenzó su estudio y exploración del origen de la vida en un rincón secreto.\nPara ese propósito, Herta la invitó a desarrollar el Universo Simulado de la mano de Tornillum y Stephen.\nEn secreto, disfruta mucho del teatro tradicional y los postres, y también está muy interesada en el bordado.',44),(45,'Es miembro de los Diez Corazones de Piedra desde muy joven y su especialidad es la recaudación de deudas.\nSu compañero, un Chanchito Dimensional llamado Conti, es capaz de percibir con agudeza dónde hay riquezas, con lo que los trabajos relacionados con seguridad, cobro de deudas y procesos actuariales no suponen ningún problema.\nActualmente, viajan juntos por el cosmos investigando diversas deudas y obligaciones que pueden afectar negativamente al desarrollo de los negocios de la Corporación.',45),(46,'Armada con una cadena de hierro y un par de dagas místicas, busca incansablemente a los criminales para capturarlos y someterlos.\nEl cuerpo que tenía se ha esfumado, así que toma prestado el cuerpo de una marioneta que puede retornar al mundo de los vivos medio día por cada criminal que atrapa.',46),(47,'La Premonición Divina, uno de los siete generales celestiales del Señor Arquero. Aunque tenga pinta de perezoso, es más meticuloso de lo que parece. No considera que salvar una situación al borde del desastre sea una muestra de sabiduría, por lo que es muy escrupuloso con los asuntos rutinarios para evitar cualquier problema potencial. Gracias a su cuidadosa gestión, Xianzhou ha disfrutado de muchos años de paz, y la apariencia aparentemente perezosa de Jing Yuan le ha hecho ganarse el apodo de \"General Somnoliento\".',47);
/*!40000 ALTER TABLE `about` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `all_characters`
--

DROP TABLE IF EXISTS `all_characters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `all_characters` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `count` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `all_characters`
--

LOCK TABLES `all_characters` WRITE;
/*!40000 ALTER TABLE `all_characters` DISABLE KEYS */;
INSERT INTO `all_characters` VALUES (1,47);
/*!40000 ALTER TABLE `all_characters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `appearance`
--

DROP TABLE IF EXISTS `appearance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `appearance` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `appearance` longtext,
  `character_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK7pvew6atotxv5202135a959qr` (`character_id`),
  CONSTRAINT `FK7pvew6atotxv5202135a959qr` FOREIGN KEY (`character_id`) REFERENCES `characters` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `appearance`
--

LOCK TABLES `appearance` WRITE;
/*!40000 ALTER TABLE `appearance` DISABLE KEYS */;
INSERT INTO `appearance` VALUES (1,'Arlan es un hombre de estatura media, delgado, de piel oscura, pelo blanco con mechones negros y ojos morados.\n\nTiene cicatrices en diferentes partes del cuerpo, pero la que más destaca es la que tiene en la nariz en forma de cruz. Lleva un chaleco negro bajo una armadura plateada. En el brazo izquierdo tiene una venda con un guante de metal, mientras que el brazo derecho está cubierto por una armadura. En la parte inferior viste unos pantalones azules con protección en la rodilla derecha con unas botas negras cortas.',1),(2,'Asta es una mujer delgada y de estatura media, de tez blanca, pelo rosa con un broche dorado y ojos violeta claro.\n\nLleva un top blanco con un moño negro y un poco de dorado con una tarjeta de identificación violeta. Con un cinturón de rayas azules y blancas, lleva una falda morada oscura con volantes blancos abierta por delante, debajo de ésta tiene otra falda negra con un poco de oro. De sus brazos cuelga una capa blanca y rosa brillante con decoración púrpura. En la pierna derecha lleva un lazo negro con decoración dorada y un lazo negro. Abajo, en los pies, lleva unos tacones negros con un lazo negro.',2),(3,'Bailu es una joven vidyadhara que usa un atuendo y mangas blancas y moradas claras junto a una falda y botas negras, y en sus tobillos tiene unas mangas azuladas. Ella tiene el cabello largo de color cerúleo claro, dividiéndose en dos colas hacia los lados, y con un mechón sobresaliendo por su izquierda hacia arriba. Por su frente emergen dos blanquecinos cuyas puntas redondas se tornan de un celeste semejante al de su pelo. Bailu tiene ojos celestes, aunque la parte de inferior de ellos es más amarilla.',3),(4,'Blade es un hombre alto y delgado, de tez blanca, de cabello color negro claro y ojos rojos.\n\nSu cabello es largo hasta la cintura y con un degrade rojo al final de este, al igual que en su flequillo. En su oreja derecha lleva un aro rojo. Viste una chaqueta larga negra con decoraciones rojas y doradas, en la parte del brazo sobresale un vendaje. Debajo de esta, aparentemente tiene una armadura dorada con pequeñas decoraciones negras y rojas. En la parte inferior tiene un pantalón gris con un pequeño vendaje que se tiñe en gris oscuro conforme va bajando. Por último, calza unos zapatos negros con suelas blancas.',4),(5,'Bronya es una mujer alta y delgada, de tez blanca, con pelo y ojos grises.\n\nLleva un vestido blanco largo con decoraciones azules y dorados. En los brazos lleva unos guantes negros largos y visibles. En las piernas lleva medias negras con botas negras de tacón con adornos azules y dorados. Debajo del vestido lleva una camiseta negra y de sus orejas cuelgan unos pendientes azules y morados.',5),(6,'Clara es una mujer pequeña y delgada, de tez blanca, pelo blanco y ojos rosados.\n\nLleva un vestido blanco bajo un abrigo rojo con adornos de color marrón claro. En su pierna derecha lleva un pequeño cinturón negro con una hebilla plateada. Va descalza.',6),(7,'Dan Heng es un hombre alto y delgado, de ojos azules, piel clara y cabello negro.\n\nDan Heng lleva un jersey negro de cuello alto con un abrigo de color blanco y turquesa con algunos diseños dorados y ahumados y una armadura de jade en el brazo izquierdo. Lleva pantalones grises y zapatos negros con suela blanca. Dan Heng también parece llevar un pendiente de borla de jade de estilo oriental y auriculares.',7),(8,'Dan Heng • Imbibitor Lunae es un joven alto y delgado con cabello negro, piel clara y ojos verdes con delineador de ojos rojo. Como Vidyadhara, tiene orejas afiladas y cuernos verde azulado como los de un dragón chino. Su vestimenta es una camisa blanca sin mangas con un corte en el pecho, con mangas largas y sueltas que tienen detalles en color dorado y verde. Lleva guantes negros sin dedos y una faja verde con un sello verde azulado rodeado de hojas doradas. Sus pantalones son negros y sus botas largas hasta la rodilla tienen detalles en blanco y dorado.',8),(9,'Fu Xuan tiene la apariencia de una mujer menuda con cabello rosado y ojos dorados. Parece tener incrustada una piedra preciosa de color amatista en su frente.\n\nLleva cuatro horquillas doradas ji, dos con borlas rosadas y adornos de luna creciente sujetos en sus coletas. Su vestimenta consiste en un vestido marrón oscuro y blanco con diseños dorados de constelaciones, junto con medias blancas que tienen patrones de nubes. También lleva un collar con volantes conectado a un adorno con borlas moradas y doradas.',9),(10,'Gepard es un hombre alto y corpulento, de piel clara, ojos azules y pelo rubio.\n\nGepard va vestido con gruesas ropas blancas con adornos azules y dorados, junto con una coraza y una armadura para su mano izquierda que parece tener diseños cristalinos azules. Lleva unas botas negras altas con botones dorados que le llegan hasta la rodilla. Parece tener una baratija de piel junto a los pantalones.',10),(11,'Herta es una mujer delgada y de tamaño medio, de tez blanca, con el pelo de color crema y los ojos morados.\n\nLleva una boina negra con una flor morada sobre su larga cabellera hasta la cola, con algunos mechones cortos alrededor de su gran flequillo. Tiene un collar morado con una llave muy cerca de su vestido blanco con cuadros grises. Encima lleva un chaleco negro hasta la rodilla con mangas separadas decoradas en morado y blanco. El chaleco se sujeta con un candado dorado alrededor de unas cadenas plateadas. Al final tiene unos tacones cubiertos de negro, blanco y morado.',11),(12,'Himeko es una mujer de mediana edad de tez blanca, pelo rojo brillante y ojos dorados a juego.\n\nLleva una toga blanca con detalles rojos en la parte inferior. El vestido lleva laureles dorados en la cintura, un cuello con una rosa dorada y una falda corta negra con flores negras colgantes como adorno. Himeko también lleva un abrigo negro con forro dorado, un cordón en el brazo izquierdo y un guante en el derecho.',12),(13,'Hook es una niña pequeña y delgada, de tez blanca, de pelo y ojos verde amarillo.\n\nEn la cabeza lleva una gran gorra marrón y blanca.\n\nEstá vestida con una camiseta blanca, con una bufanda amarilla decorada con un lazo amarillo-naranja. Encima lleva un abrigo marrón y guantes marrones. En las piernas lleva unas botas negras con un rojo claro, y en la rodilla izquierda tiene un protector plateado con forma de gato.',13),(15,'Kafka es una mujer joven con cabello color vino tinto que se ata en una coleta desordenada, con dos flequillos sueltos colgando a cada lado. Sus ojos son de un color similar, más claro. Tiene gafas de sol oscuras quevedas en la parte superior de la cabeza y un arete de perla en la oreja derecha.\n\nViste una camisa de vestir blanca junto con una chaqueta negra que cuelga de sus hombros. Hay un broche de mariposa plateado en su solapa izquierda. En la espalda hay un gran patrón en forma de araña en el centro, junto con telarañas en ambos hombros, con un interior de color burdeos. Hay correas de color sangría con detalles dorados tanto en la chaqueta como en los muslos, y guantes de un tono similar.\n\nViste pantalones cortos negros de cintura alta y medias de nailon, con una liga en el muslo en la pierna derecha. Ella también usa dos botas negras de diferentes longitudes: sobre la rodilla en la pierna derecha, ligeramente sobre el tobillo en la izquierda.',15),(16,'Luka tiene la apariencia de un hombre de tez clara y esbelto, con cabello rojo brillante y ojos azules. Parece tener un brazo derecho robótico.\n\nSu vestimenta consiste en un chaleco rojo con un emblema de llamas plateadas incrustado en uno de los bordes, una camiseta blanca rasgada y debajo parece haber una camiseta sin mangas con diseños de lava. Lleva una banda de brazo negra y un guante negro en su brazo izquierdo. Sus prendas inferiores consisten en pantalones marrones, zapatos negros, un cinturón rojo con una hebilla dorada diseñada con un emblema de puño plateado, varios sujetadores de muslo y una armadura negra larga en su pierna izquierda, y una armadura roja corta en su pierna derecha.',16),(17,'Luocha es un hombre alto y delgado, de tez blanca, cabello rubio hasta la cintura y ojos verdes.\n\nVa vestido con un traje blanco largo con adornos verde agua y dorados y una camisa negra. Lleva botas negras a juego con sus guantes negros.',17),(18,'Al igual que la mayoría de sus hermanos, Lynx es una chica de cabello rubio con ojos azulados. Su tocado es una capucha con orejas de lince. Lynx lleva una chaqueta blanca y de color aguamarina que tiene un ribete de piel. También lleva un par de guantes negros hasta el codo y una bufanda azul con diseño de copos de nieve. Su atuendo inferior consiste en una malla azul y un par de botas con ribete de piel.',18),(19,'Natasha es una mujer alta y delgada, de tez blanca, con pelo azul y ojos rosados.\n\nLleva un collar con una botella redonda con un líquido verde brillante.\n\nLleva un vestido blanco en las mangas y la espalda, negro en los pechos y azul en la parte delantera. En los brazos lleva unos largos guantes marrones transparentes con guantes negros. Lo mismo ocurre con las pantimedias marrones que lleva debajo de las largas botas negras.',19),(20,'Pela es una mujer de estatura media, con ojos azules y pelo morado con flequillo. Lleva un uniforme de la Guardia Crinargenta que consiste en una boina negra, un vestido blanco con adornos dorados y numerosos broches alrededor, y un par de guantes de codo. También lleva unas gafas grandes y redondas. Su vestimenta consiste en unas mallas negras y un par de botas blancas con cintas azules y blancas.',20),(21,'Qingque es una mujer de baja estatura con cabello beige de longitud media y ojos verdes. Su cabello está recogido en dos coletas bajas que están trenzadas sobre la base. Ambas coletas están sujetas con gomas para el pelo negras, con una decoración adicional que presenta un pájaro blanco y una cinta negra atada a la coleta izquierda.\n\nLleva un top de cuello alto sin hombros de punto negro con un corpiño verde que se extiende hacia una levita, decorada con motivos de nubes y pájaros. El cuello alto tiene un lazo negro en la parte posterior. Hay un broche dorado y un borlón rojo en la cintura. Secciones atadas y enlazadas en las mangas y la espalda tienen lazos verdes y dobles borlas con adornos de estrellas adjuntos, respectivamente. Se puede ver una correa roja en su hombro derecho, y tiene un gemelo en ambos puños verdes plisados de sus mangas. Debajo del top hay una falda plisada verde y blanca de doble capa con un diseño de pájaros verde claro que llega a sus muslos superiores. Debajo de sus calcetines blancos hay botines negros con suela blanca adornados con un volante verde y una cremallera dorada.',21),(22,'Sampo es un hombre humano alto, delgado y de piel clara. Tiene cabello azul oscuro de longitud media con las puntas de gris pizarra en el cuello, y flequillo largo barrido sobre su ojo izquierdo. Sus ojos están ligeramente inclinados hacia abajo y son de color verde menta con pupilas felinas. Lleva un brazalete plateado en su oreja derecha. Su vestimenta superior incluye varias capas, la más exterior es una chaqueta de color magenta parecida a un traje con detalles blancos y morados, mangas cortas y largas colas. Debajo de la chaqueta lleva una camisa negra asimétrica con el frente abierto y un alto cuello de vestir. Debajo de la camisa hay un chaleco gris con marcas gris oscuro y recortes en cada cadera que dejan al descubierto su piel. Cinturones negros cruzan tanto su abdomen como su pecho. En el hombro derecho lleva un espaldarón plateado con púas. Lleva guantes negros y magenta desparejados con detalles de metal plateado. Sus pantalones son de un tono azul grisáceo oscuro con un cinturón negro y numerosas correas en cada pierna, y lleva zapatos de vestir marrones y negros adornados con ornamentos de metal plateado.',22),(23,'Seele es una joven mujer con cabello largo de color morado y ojos morado claro. Lleva una bufanda morada con detalles rojos y blancos, así como un conjunto blanco, negro y morado con pantalones cortos negros. En su brazo izquierdo lleva una banda roja atada junto con una manga morada, y en el derecho un guante sin dedos que llega hasta el codo. También lleva un par de botas negras y azules de diferentes longitudes.',23),(24,'Serval es una mujer alta con ojos azules y cabello rubio que tiene mechones de color azul oscuro y puntas de color azul claro en los extremos. Tiene un solo arete de argolla, un collar negro, esmalte de uñas negro y un tatuaje de un rayo morado en su cintura.\n\nLleva lo que parece ser un top negro cosido junto con la mitad de un guardapolvo blanco. El guardapolvo llega hasta su tobillo y tiene un agujero quemado cerca del final. Lleva mangas separadas que coinciden con el color de cada lado de su top, con dos cinturones envueltos alrededor de su manga negra y un broche de pluma azul en su manga blanca. Tiene guantes sin dedos desparejados, con un guante de cuero negro en su mano derecha y un guante de tela rosa en su mano izquierda.\n\nTambién lleva una minifalda azul real con un recorte sobre un par de pantalones cortos azul marino, sujetos con un cinturón de cuero negro. Lleva un juego de medias, con una media negra que llega hasta el muslo en su pierna derecha y botas negras con ribete dorado en el tobillo.',24),(25,'Siete de Marzo es una mujer joven con el pelo rosa claro, tez clara y ojos multicolores a juego de degradado azul y rosa.\n\nViste un top blanco con mangas degradadas en azul y un poco de violeta, lleva un collar negro y una falda azul con capas. Su cámara está sujeta a un chaleco negro cerca de la falda. Lleva zapatos negros y un lazo blanco en la pierna derecha. En el juego se la ve con guantes de arquero en la mano derecha.',25),(26,'Silver Wolf es una mujer delgada y de tamaño medio, de tez blanca con cabello y ojos plateados.\n\nLleva una gran coleta teñida de azul al final, sobre el flequillo lleva unas gafas aparentemente de protección. Va vestida con una chaqueta corta negra y azul, bajo la cual lleva una camiseta corta blanca. En la parte inferior lleva un pantalón corto con un cinturón blanco con bolsillo y varios adornos, en la pierna derecha tiene una tirita azul claro. Sus botas son negras con adornos azules. En sus manos lleva unos guantes sin dedos.',26),(27,'Sushang es una mujer alta y delgada, de tez blanca, pelo castaño y ojos ámbar.\n\nLleva un vestido blanco con mangas de color ámbar bajo un largo chaleco negro que deja al descubierto su pecho. En la parte inferior lleva unas botas de tacón negras y doradas. En la parte superior de la bota izquierda lleva un lazo negro.',27),(28,'Tingyun es una joven mujer Raposiana con ojos verdes, una cola marrón peluda, cabello marrón con puntas rojas recogido en una coleta, y grandes orejas de zorro marrón. Lleva un vestido marrón sin mangas con detalles blancos y rojos, junto con una faja roja que actúa como cinturón, una pulsera de jade en su muñeca izquierda y mangas cortas desprendidas a juego. Para calzado, lleva polainas blancas hasta la pantorrilla y sandalias doradas que dejan los dedos al descubierto.',28),(29,'Caelus, lleva una camisa blanca interior, pantalones negros y una chaqueta negra de manga larga.',29),(30,'Estela, lleva una camisa y chaqueta (aunque con las mangas de la chaqueta enrolladas), una falda negra ajustada y una liga azul claro en el muslo izquierdo. ',30),(31,'Caelus, lleva una camisa blanca interior, pantalones negros y una chaqueta negra de manga larga.',31),(32,'Estela, lleva una camisa y chaqueta (aunque con las mangas de la chaqueta enrolladas), una falda negra ajustada y una liga azul claro en el muslo izquierdo. ',32),(33,'Welt es un hombre alto con el pelo castaño con un pequeño mechón blanco a lo largo del flequillo y ojos marrones a juego.\n\nWelt lleva un abrigo gris y blanco con una bufanda y una armadura aparentemente negra en el pecho. Lleva gafas, pantalones marrón oscuro y zapatos negros. Su brazo izquierdo parece llevar un guante negro.',33),(34,'Yanqing es un adolescente con cabello rubio de longitud media peinado en una coleta con flequillo cortina. Tiene ojos amarillo-ámbar y un único pendiente negro en su oreja izquierda. Motivos de nubes y golondrinas, así como una marcada asimetría, aparecen con frecuencia en su atuendo.\n\nYanqing lleva una túnica hanfu azul y negra con detalles en oro y rosa, con mangas acampanadas que llegan justo por encima del codo. Una flauta de bambú turquesa está sujeta a su cinturón, junto con una pequeña bolsa rosa. Una serie de cintas rojas atan un amuleto de golondrina en su brazo izquierdo, justo debajo de la franja de tela azul pálido que cuelga sobre su hombro izquierdo, sujeta por un amuleto de bloqueo. Las colas del hanfu muestran patrones de nubes rosas y blancas.\n\nBajo la capa exterior, lleva una camisa blanca y pantalones cortos tipo tangzhuang, con una armadura negra y un cinturón azul con un broche de golondrina atados alrededor de la cintura. Ambos brazos están protegidos con brazaletes negros sin dedos, con el izquierdo con la adición de un cascabel alrededor de la muñeca. En su tobillo derecho, sobre sus botas blancas, hay un tobillera roja de cordón.',34),(35,'Yukong tiene la apariencia de una mujer Raposiana con cabello verde azulado y ojos índigo.',35),(36,'Argenti es descrito como un hombre con cabello rojo que llega hasta la mitad de la espalda y dos mechones que le llegan al pecho. Tiene ojos verdes, pupilas de color rosado rojizo y algunos destellos en sus ojos.\n\nLleva puesto un atuendo de caballero, que consiste en un traje negro por dentro, una coraza plateada claro con puntas doradas, guanteletes, musleras, brazales, escarpines y espaldarones, el izquierdo está moldeado como una rosa en el centro con detalles dorados en forma de alas en los costados. También lleva un corsé rojo por dentro con una tela roja que está envuelta alrededor de su cintura pero tiene una gran abertura en la parte posterior, dos aberturas a los lados, y también tiene puntas doradas en cada extremo de la tela, junto con una delgada tela roja que llega hasta su muslo, colgando de su hombro izquierdo con un adorno dorado y otro grande al final de la tela.',36),(37,'Cisne Negro es una mujer alta con piel pálida, cabello largo de color lavanda claro que llega hasta los muslos y se desvanece en un tono más oscuro y está ligeramente rizado en las puntas, y ojos violeta mezclados con colores amarillos, naranjas y rojos.\n\nLleva un velo grueso corto de color púrpura con un cinturón en la cabeza. Su atuendo es un traje de cuerpo morado con acentos negros y un escote halter, unido a su gargantilla de encaje. Su ropa morada tiene dos ventanas en los lados y una gran abertura en la parte delantera, revelando una prenda similar a un leotardo de color negro que tiene una ventana en su estómago, con todas las áreas expuestas cubiertas por una fina tela negra. Lleva guantes largos morados que llegan hasta sus bíceps, de color negro en su mano y tiene una pulsera dorada en su muñeca derecha. En sus piernas, lleva botas negras de tacón alto hasta el muslo con cinturones negros atados alrededor de sus muslos. Su atuendo tiene estrellas de cuatro puntas en su torso, que van de rosa a azul, una estrella de cuatro puntas en su pulsera, guantes, botas y en el borde de su velo.',37),(38,'Dr. Ratio tiene cabello violeta ondulado que cubre parcialmente su ojo izquierdo. Tiene piel pálida y una constitución muscular, con ojos de color rosado rojizo con un anillo amarillo alrededor de las pupilas. Lleva un chaleco negro con un recorte en forma de diamante en el frente y otro recorte en su costado, botones blancos en el medio, con un adorno dorado en la parte superior con una joya morada en su centro. Tiene pantalones azul oscuro, con sandalias doradas y brazaletes de brazo con gemas azules. Lleva una tela azul alrededor de su hombro derecho y una tela blanca sin mangas alrededor de su izquierdo, sostenida por un cinturón negro en su cintura. Hay correas envueltas alrededor de su muslo y su brazo.\n\nSu pieza dorada en el hombro se asemeja a la cabeza del búho, con motivos de alas, plumas y cabeza de búho en todo su atuendo. Esto, combinado con el accesorio similar a una corona de laurel en su cabeza, apunta a la sabiduría y logros académicos del Dr. Ratio.\n\nDe vez en cuando lleva una escultura de alabastro blanco sobre su cabeza, con un peinado diferente y una corona de laurel.',38),(39,'Guinaifen tiene la apariencia de una joven mujer con cabello largo de color rosa anaranjado que está recogido en lo alto de su cabeza a la izquierda, con una flor de loto roja y un mechón desde atrás sobre su hombro derecho, también tiene ojos dorados con pupilas blancas en forma de cometa.\n\nLleva un top corto rojo sin tirantes que tiene contornos dorados y está unido a una gargantilla por dos correas negras, con mangas rojas sueltas que tienen un volante de color verde azulado oscuro en la parte superior y una tela blanca en la parte inferior que es del mismo estilo que su falda, un borlón dorado en su manga derecha, un corsé negro que tiene un adorno dorado en forma de cometa, un lazo rojo que tiene ribetes dorados sujeto a su cadera izquierda, guantes negros que dejan al descubierto su pulgar, accesorios negros en sus muñecas y una falda corta blanca, junto con seis colas rojas que son del mismo estilo que su top corto, con puntas y ribetes dorados, y un adorno dorado en forma de cometa que está sujeto a una tela roja en forma de óvalo que cuelga de las dos últimas colas largas.\n\nTambién lleva medias altas negras, una ligera negra en su muslo derecho con un adorno dorado en el medio y botas de tacón negro que tienen puntas doradas y una suela blanca.',39),(40,'Hanya es una joven con cabello de color gris azul claro que llega hasta los muslos y está atado en una cola de caballo baja con un lazo de color azul marino oscuro que se une al rojo, tiene dos mechones que llegan hasta su clavícula y un poco de flequillo que le llega hasta la nariz en el centro. También tiene ojos grises y pupilas azules, junto con un poco de sombra en sus ojeras. En su cabeza lleva un accesorio de color azul marino oscuro que tiene una punta roja, apuntando hacia arriba y hacia adelante, y casi parece alas de cabeza, el accesorio izquierdo tiene un borlón rojo con una punta cian colgando de él.\n\nLleva un maillot negro sin mangas de cuello alto simulado, un cuello de encaje negro que está unido al maillot con una franja roja en la parte trasera, un gargantilla dorada con un adorno grande cian colgando de ella, y guantes con accesorios dorados en las manos. También lleva un vestido azul marino oscuro sin tirantes con un recorte en el estómago y la cintura y una abertura en el pecho, con una banda azul marino claro alrededor de la cintura que llega hasta las caderas, tiene un adorno dorado en forma de girasol en el centro y un adorno dorado en la parte posterior, ambos sujetos a un cinturón negro en la banda. Tiene una tela azul marino oscuro que llega hasta su muslo colgando de cada uno de sus brazales de brazo negros, junto con dos telas negras con puntas rojas colgando de su banda, llegando hasta su tobillo, y dos telas azul marino oscuro en el medio, una al frente y llega hasta su rodilla, y la otra en la parte trasera llega hasta su tobillo. También tiene un tatuaje negro en su muslo izquierdo y lleva sandalias negras que dejan sus dedos al descubierto con esmalte de uñas negro, tienen suelas azules y un grueso brazalete dorado en el tobillo.',40),(41,'Huohuo es una chica Foxiana con cabello verde salvia que llega hasta los codos, con puntas más claras que recuerdan a la cola de un zorro. A diferencia de la mayoría de los Foxianos, tiene orejas dobladas, con su oreja derecha mostrando una muesca. Tiene ojos verdes redondos ligeramente inclinados hacia abajo con pupilas en forma de huella de pata blanca y acentos amarillos. Se pueden ver ojeras bajo sus ojos.\n\nEl sombrero de Huohuo es de color azul oscuro y beige, con un adorno dorado pálido que sostiene una pequeña bandera amarilla en el lado derecho. Un hexagrama de fuego del I Ching adorna la parte frontal, mientras que adornos dorados claros en forma de diamantes cuelgan de los paneles laterales del sombrero detrás de sus orejas. Lleva una camisa con estampado de nubes verde y blanco con mangas desprendidas. Un borlón rojo que sostiene un adorno azul claro está fijo en su cuello blanco plegado. Sobre la camisa hay un abrigo delgado de color azul oscuro con detalles blancos y beige que es lo suficientemente largo como para llegar a sus rodillas. Huohuo lleva pantalones cortos con bordes de color beige y verde, polainas blancas con borlas dorado-verdes y zapatos planos de color verde oscuro.\n\nCuando Tail está adjunta a ella, tiene una cola esponjosa de color cian con un talismán en forma de X amarillo pegado sobre ella. Si Tail se desprende, la cola cian desaparece y es reemplazada por un talismán en forma de X amarillo más grande que cubre donde solía estar su cola.',41),(42,'Jingliu tiene la apariencia de una mujer esbelta con cabello pálido largo y ojos rojos.\n\nLleva una venda negra con un detalle de luna creciente plateada en el medio. Su vestimenta consiste en un elegante vestido con una paleta de colores negro, azul oscuro y blanco con diseños de fases lunares, junto con una cinta azul en su cabello y una cinta roja en la cadera y la parte trasera del vestido, además de algunos detalles de nubes. También lleva botas oscuras.',42),(43,NULL,43),(44,'Ruan Mei es una joven con ojos turquesa, piel clara y cabello largo castaño con mechones verde oscuro que está suelto detrás de su cabeza con un pasador para el cabello. Lleva un pendiente en su oreja izquierda y un collar de perlas alrededor de su cuello.\n\nViste un elegante vestido en tonos verde oscuro, marrón y blanco con detalles en dorado, guantes verdes oscuro que llegan hasta los codos y un brazalete plateado en su muñeca izquierda. También lleva flores en el lado central-izquierdo de sus caderas. En su muslo derecho lleva una liga verde oscuro en forma de doble hélice de ADN con una flor en el centro. También lleva tacones altos negros con patrones dorados.',44),(45,'Topaz tiene ojos azules y cabello plateado blanco con una raya roja en su flequillo y todo rojo en el interior.\n\nLleva una chaqueta blanca sin mangas que está desabotonada en la zona del pecho, un collar negro en el cuello de su chaqueta, con un blazer negro sin mangas que está unido a unos pantalones cortos negros de talle alto con un cinturón negro, una tela roja que cuelga de su hombro izquierdo que llega hasta sus rodillas, con un diamante y una tiara en la parte superior del detalle dorado en la tela, una medalla dorada en su pecho izquierdo con una tela blanca colgando de ella.\n\nLleva unos pantalones cortos ajustados blancos que tienen la misma longitud que sus pantalones cortos de talle alto, y una abertura en su muslo derecho, junto con un lazo rojo atado en su abertura de muslo, una manga negra desprendida que va bajo su codo, con una pulsera azul que tiene puntas doradas, un guante ajustado negro que tiene la misma longitud que su manga, y su guante izquierdo es corto, una liga roja en su muslo derecho con una cadena de cuerda dorada, y botas de tacón negro.',45),(46,NULL,46),(47,'Jing Yuan es un hombre alto y delgado, de tez blanca, pelo blanco y ojos amarillos.\n\nLleva el pelo recogido en una coleta roja de la que caen largos mechones alrededor del flequillo. Lleva una camiseta blanca bajo una armadura dorada. En las manos lleva unos guantes marrones sin dedos.\n\nEn la parte inferior lleva pantalones rojos con botas marrones.',47);
/*!40000 ALTER TABLE `appearance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `characters`
--

DROP TABLE IF EXISTS `characters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `characters` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `category` varchar(255) DEFAULT NULL,
  `element` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `rarity` varchar(255) DEFAULT NULL,
  `voice` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `characters`
--

LOCK TABLES `characters` WRITE;
/*!40000 ALTER TABLE `characters` DISABLE KEYS */;
INSERT INTO `characters` VALUES (1,'Humano','Rayo','Masculino','Estación Espacial Herta','Arlan','Destrucción','✦ ✦ ✦ ✦','Ryoko Shiraishi'),(2,'Humana','Fuego','Femenino','Estación Espacial Herta','Asta','Armonía','✦ ✦ ✦ ✦','Akasaki Chinatsu'),(3,'Vidyadhara','Rayo','Femenino','El Luofu de Xianzhou','Bailu','Abundancia','✦ ✦ ✦ ✦ ✦','Emiri Katō'),(4,'Humano','Viento','Masculino','Cazadores de Estelaron','Blade','Destrucción','✦ ✦ ✦ ✦ ✦','Miki Shinichiro'),(5,'Humana','Viento','Femenino','Belobog','Bronya','Armonía','✦ ✦ ✦ ✦ ✦','Asumi Kana'),(6,'Humana','Físico','Femenino','Belobog','Clara','Destrucción','✦ ✦ ✦ ✦ ✦','Hidaka Rina'),(7,'Humano','Viento','Masculino','Expreso Astral','Dan Heng','Cacería','✦ ✦ ✦ ✦','Ito Kento'),(8,'Vidyadhara','Imaginario','Masculino','El Luofu de Xianzhou','Dan Heng Imbibitor Lunae','Destrucción','✦ ✦ ✦ ✦ ✦','Kent Itō'),(9,'Humana','Cuántico','Femenino','El Luofu de Xianzhou','Fu Xuan','Conservación','✦ ✦ ✦ ✦ ✦','Miku Itō'),(10,'Humano','Hielo','Masculino','Belobog','Gepard','Conservación','✦ ✦ ✦ ✦ ✦','Furukawa Makoto'),(11,'Humana','Hielo','Femenino','Estación Espacial Herta','Herta','Erudición','✦ ✦ ✦ ✦','Yamazaki Haruka'),(12,'Humana','Fuego','Femenino','Expreso Astral','Himeko','Erudición','✦ ✦ ✦ ✦ ✦','Tanaka Rie'),(13,'Humana','Fuego','Femenino','Belobog','Hook','Destrucción','✦ ✦ ✦ ✦','Tokui Sora'),(15,'Humana','Rayo','Femenino','Cazadores de Estelaron','Kafka','Nihilidad','✦ ✦ ✦ ✦ ✦','Itoh Shizuka'),(16,'Humano','Físico','Masculino','Belobog','Luka','Nihilidad','✦ ✦ ✦ ✦','Gakuto Kajiwara'),(17,'Humano','Imaginario','Masculino','El Luofu de Xianzhou','Luocha','Abundancia','✦ ✦ ✦ ✦ ✦','Ishida Akira'),(18,'Humana','Cuántico','Femenino','Belobog','Lynx','Abundancia','✦ ✦ ✦ ✦','Haruka Terui'),(19,'Humana','Físico','Femenino','Belobog','Natasha','Abundancia','✦ ✦ ✦ ✦','Uchiyama Yumi'),(20,'Humana','Hielo','Femenino','Belobog','Pela','Nihilidad','✦ ✦ ✦ ✦','Morohoshi Sumire'),(21,'Humana','Cuántico','Femenino','El Luofu de Xianzhou','Qingque','Erudición','✦ ✦ ✦ ✦','Arisa Date'),(22,'Humano','Viento','Masculino','Belobog','Sampo','Nihilidad','✦ ✦ ✦ ✦','Hirakawa Daisuke'),(23,'Humana','Cuántico','Femenino','Belobog','Seele','Cacería','✦ ✦ ✦ ✦','Nakahara Mai'),(24,'Humana','Rayo','Femenino','Belobog','Serval','Erudición','✦ ✦ ✦ ✦','Aimi'),(25,'Humana','Hielo','Femenino','Expreso Astral','Siete de Marzo','Conservación','✦ ✦ ✦ ✦','Ogura Yui'),(26,'Humana','Cuántico','Femenino','Cazadores de Estelaron','Silver Wolf','Nihilidad','✦ ✦ ✦ ✦ ✦','Asumi Kana'),(27,'Humana','Físico','Femenino','El Luofu de Xianzhou','Sushang','Cacería','✦ ✦ ✦ ✦','Fukuen Misato'),(28,'Raposiano','Rayo','Femenino','El Luofu de Xianzhou','Tingyun','Armonía','✦ ✦ ✦ ✦','Yūki Takada'),(29,'Humano','Físico','Masculino','Expreso Astral','Caelus (Fisico)','Destrucción','✦ ✦ ✦ ✦ ✦','Enoki Junya'),(30,'Humana','Físico','Femenino','Expreso Astral','Estela (Fisico)','Destrucción','✦ ✦ ✦ ✦ ✦','Ishikawa Yui'),(31,'Humano','Fuego','Masculino','Expreso Astral','Caelus (Fuego)','Conservación','✦ ✦ ✦ ✦ ✦','Enoki Junya'),(32,'Humana','Fuego','Femenino','Expreso Astral','Estela (Fuego)','Conservación','✦ ✦ ✦ ✦ ✦','Ishikawa Yui'),(33,'Humano','Imaginario','Masculino','Expreso Astral','Welt','Nihilidad','✦ ✦ ✦ ✦ ✦','Hosoya Yoshimasa'),(34,'Humano','Hielo','Masculino','El Luofu de Xianzhou','Yanqing','Cacería','✦ ✦ ✦ ✦ ✦','Marina Inoue'),(35,'Raposiano','Imaginario','Femenino','El Luofu de Xianzhou','Yukong','Armonía','✦ ✦ ✦ ✦','Yumi Tōma'),(36,'Humano','Físico','Masculino','Caballeros de la Belleza','Argenti','Erudición','✦ ✦ ✦ ✦ ✦','Shinnosuke Tachibana'),(37,'Humana','Viento','Femenino','Penacony','Cisne Negro','Nihilidad','✦ ✦ ✦ ✦ ✦','Hitomi Nabatame'),(38,'Humano','Imaginario','Masculino','Sociedad del Conocimiento','Dr. Ratio','Cacería','✦ ✦ ✦ ✦ ✦','Shunsuke Takeuchi'),(39,'Nativo de Xianzhou','Fuego','Femenino','El Luofu de Xianzhou','Guinaifen','Nihilidad','✦ ✦ ✦ ✦','Hina Suguta'),(40,'Nativo de Xianzhou','Físico','Femenino','El Luofu de Xianzhou','Hanya','Armonía','✦ ✦ ✦ ✦','Sayumi Suzushiro'),(41,'Raposiano','Viento','Femenino','El Luofu de Xianzhou','Huohuo','Abundancia','✦ ✦ ✦ ✦ ✦','Maria Naganawa'),(42,'Nativo de Xianzhou','Hielo','Femenino','El Luofu de Xianzhou','Jingliu','Destrucción','✦ ✦ ✦ ✦ ✦','Houko Kuwashima'),(43,'Humano','Hielo','Masculino','Penacony','Misha','Destrucción','✦ ✦ ✦ ✦','Eriko Matsui'),(44,'Humana','Hielo','Femenino','Estación Espacial Herta','Ruan Mei','Armonía','✦ ✦ ✦ ✦ ✦','Saori Ōnishi'),(45,'Humana','Fuego','Femenino','Corporación para la Paz Interastral','Topaz y Conti','Cacería','✦ ✦ ✦ ✦ ✦','Houko Kuwashima'),(46,'Marioneta','Cuántico','Femenino','El Luofu de Xianzhou','Xueyi','Destrucción','✦ ✦ ✦ ✦','Maki Kawase'),(47,'Humano','Rayo','Masculino','El Luofu de Xianzhou','Jing Yuan','Erudición','✦ ✦ ✦ ✦ ✦','Ono Daisuke');
/*!40000 ALTER TABLE `characters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dialogues`
--

DROP TABLE IF EXISTS `dialogues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dialogues` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `conversation` longtext,
  `a_data` longtext,
  `about_self` longtext,
  `farewell` longtext,
  `first_meeting` longtext,
  `greeting` longtext,
  `hobbies` longtext,
  `inconvenience` longtext,
  `something_to_share` longtext,
  `character_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKe1p3ddd7e3i99c7bams1gxb6r` (`character_id`),
  CONSTRAINT `FKe1p3ddd7e3i99c7bams1gxb6r` FOREIGN KEY (`character_id`) REFERENCES `characters` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dialogues`
--

LOCK TABLES `dialogues` WRITE;
/*!40000 ALTER TABLE `dialogues` DISABLE KEYS */;
/*!40000 ALTER TABLE `dialogues` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eidolons`
--

DROP TABLE IF EXISTS `eidolons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `eidolons` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `description1` longtext,
  `description2` longtext,
  `description3` longtext,
  `description4` longtext,
  `description5` longtext,
  `description6` longtext,
  `edilons1image` longtext,
  `edilons2image` longtext,
  `edilons3image` longtext,
  `edilons4image` longtext,
  `edilons5image` longtext,
  `edilons6image` longtext,
  `eidolons1` longtext,
  `eidolons2` longtext,
  `eidolons3` longtext,
  `eidolons4` longtext,
  `eidolons5` longtext,
  `eidolons6` longtext,
  `character_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK3pj2doo80hvwvcrng300w51kb` (`character_id`),
  CONSTRAINT `FK3pj2doo80hvwvcrng300w51kb` FOREIGN KEY (`character_id`) REFERENCES `characters` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eidolons`
--

LOCK TABLES `eidolons` WRITE;
/*!40000 ALTER TABLE `eidolons` DISABLE KEYS */;
/*!40000 ALTER TABLE `eidolons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `liststats`
--

DROP TABLE IF EXISTS `liststats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `liststats` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `character_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK56w2ut4q0pimuqt5cwhtsk1g4` (`character_id`),
  CONSTRAINT `FK56w2ut4q0pimuqt5cwhtsk1g4` FOREIGN KEY (`character_id`) REFERENCES `characters` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `liststats`
--

LOCK TABLES `liststats` WRITE;
/*!40000 ALTER TABLE `liststats` DISABLE KEYS */;
/*!40000 ALTER TABLE `liststats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `maxstats`
--

DROP TABLE IF EXISTS `maxstats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `maxstats` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `atk` varchar(255) DEFAULT NULL,
  `crit_dmg` varchar(255) DEFAULT NULL,
  `crit_rate` varchar(255) DEFAULT NULL,
  `def` varchar(255) DEFAULT NULL,
  `energy` varchar(255) DEFAULT NULL,
  `hp` varchar(255) DEFAULT NULL,
  `spd` varchar(255) DEFAULT NULL,
  `taunt` varchar(255) DEFAULT NULL,
  `list_stats_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKl93yv4yixgqms9s1omo1qkbmr` (`list_stats_id`),
  CONSTRAINT `FKl93yv4yixgqms9s1omo1qkbmr` FOREIGN KEY (`list_stats_id`) REFERENCES `liststats` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `maxstats`
--

LOCK TABLES `maxstats` WRITE;
/*!40000 ALTER TABLE `maxstats` DISABLE KEYS */;
/*!40000 ALTER TABLE `maxstats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `minstats`
--

DROP TABLE IF EXISTS `minstats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `minstats` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `atk` varchar(255) DEFAULT NULL,
  `crit_dmg` varchar(255) DEFAULT NULL,
  `crit_rate` varchar(255) DEFAULT NULL,
  `def` varchar(255) DEFAULT NULL,
  `energy` varchar(255) DEFAULT NULL,
  `hp` varchar(255) DEFAULT NULL,
  `spd` varchar(255) DEFAULT NULL,
  `taunt` varchar(255) DEFAULT NULL,
  `list_stats_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK5834k6941w0lsbprj08osiea0` (`list_stats_id`),
  CONSTRAINT `FK5834k6941w0lsbprj08osiea0` FOREIGN KEY (`list_stats_id`) REFERENCES `liststats` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `minstats`
--

LOCK TABLES `minstats` WRITE;
/*!40000 ALTER TABLE `minstats` DISABLE KEYS */;
/*!40000 ALTER TABLE `minstats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profile`
--

DROP TABLE IF EXISTS `profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `profile` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `profile` longtext,
  `character_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKaqepce7k550rdtrw0pachuyr7` (`character_id`),
  CONSTRAINT `FKaqepce7k550rdtrw0pachuyr7` FOREIGN KEY (`character_id`) REFERENCES `characters` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profile`
--

LOCK TABLES `profile` WRITE;
/*!40000 ALTER TABLE `profile` DISABLE KEYS */;
INSERT INTO `profile` VALUES (1,'Responsable del Departamento de Seguridad de la Estación Espacial Herta. Un joven no muy elocuente que protege a los investigadores para que puedan completar su trabajo.',1),(2,'Investigadora jefa de la Estación Espacial Herta. Una joven aristocrática procedente de una familia muy conocida. Una astrónoma que siente curiosidad por todo y es experta a la hora de gestionar los diversos empleados de la Estación Espacial.',2),(3,'Gran Maestra de los vidyadhara, también conocida como Dama Sanadora en el Luofu de Xianzhou. Utiliza sus conocimientos médicos únicos y los tratamientos que solo puede proporcionar la raza dracónica de los vidyadhara para salvar vidas',3),(4,'Miembro de los Cazadores de Estelaron y espadachín que abandonó su cuerpo para convertirse en espada. Juró lealtad al Esclavo del Destino y posee una terrorífica capacidad de autocuración.',4),(5,'Heredera de la Guardiana Suprema de Belobog. Es orgullosa como una princesa, pero también íntegra y leal como un soldado.',5),(6,'Niña vagabunda que vive con robots. Es introvertida y gentil y tiene un corazón puro. Desea que todos los habitantes del Bajomundo sean una gran familia.',6),(7,'Un joven distante y reservado que guarda un secreto sobre su pasado. Decidió viajar con el Expreso Astral para escapar de sus familiares.',7),(8,'Gran Maestro del Luofu, portador del legado del Dragón Azul. Quien envía nubes de lluvia y se encarga de vigilar el inmortal Árbol de la Ambrosía. Porta el título de Imbibitor Lunae.\nHistoria de Xianzhou: La migración de los cinco dragones',8),(9,'Dirigente de la Comisión de Adivinación del Luofu de Xianzhou. Es quien utiliza el tercer ojo y la Matriz del Presagio para predecir la ruta de Xianzhou y los resultados de los acontecimientos.',9),(10,'Capitán de la Guardia Crinargenta y guerrero notorio de Belobog. Es tal y como aparenta ser, muy meticuloso y dedicado.',10),(11,'Miembro n.º 83 del Círculo de Genios y verdadera dueña de la Estación Espacial. Una científica extremadamente inteligente y absolutamente carente de empatía.',11),(12,'La restauradora del Expreso Astral. Decidió viajar con el Expreso Astral para contemplar el vasto cielo estrellado. Es aficionada a preparar café artesanal.',12),(13,'Jefa autoproclamada del grupo de aventura del Bajomundo llamado Los Topos. Ama la libertad y cree que la vida es una sucesión de aventuras.',13),(15,'Es miembro de los Cazadores de Estelaron. y es tranquila, serena y hermosa. Su registro en la lista de buscados de la Corporación para la Paz Interastral solo incluye su nombre y su pasatiempo. La gente siempre la ha imaginado elegante, respetable y en busca de cosas bellas, incluso en combate.',15),(16,'Campeón de lucha del Bajomundo de Belobog y uno de los mejores combatientes de Llamarada. El varias veces campeón del Club de la Pelea inspira a los niños del Bajomundo con su entusiasmo.',16),(17,'Comerciante extranjero que vino de otra galaxia y que siempre lleva consigo un ataúd. Posee unos conocimientos médicos excelentes.',17),(18,'Es la hermana menor de Serval y Gepard. Se aleja de las interacciones sociales y es la mejor exploradora de Belobog.',18),(19,'Doctora del Bajomundo y cuidadora de niños. Tiene un carácter dulce y amable, pero oculta un lado peligroso.',19),(20,'Admirada por la Guardia Crinargenta, es una oficial de inteligencia que maneja sus asuntos. Puede responder a cualquier problema con tranquilidad y seguridad.',20),(21,'Aeroembajadora de la Comisión del Transporte Celeste del Luofu de Xianzhou. Viaja con delegados empresariales y crea relaciones comerciales y alianzas con muchos mundos.',21),(22,'Hombre de negocios que viaja entre la superficie y el subsuelo. Es cercano con todo el mundo, tiene un gran sentido del humor y le gusta bromear.',22),(23,'Miembro importante de Llamarada, la organización de resistencia del Bajomundo. Su nombre en clave es Babochka. Es muy franca y directa, pero también esconde un lado delicado en su interior.',23),(24,'Mecánica de Belobog. Antes era investigadora de la División de Tecnología de los Arquitectos. Es la hermana mayor de Gepard Landau y sus personalidades no pueden ser más distintas. Le encanta un antiguo género de música, de antes del Hielo Eterno, conocido como \"rock\".',24),(25,'Una muchacha entusiasta que dormía en el hielo sin fin y que no sabe nada sobre su pasado. Decidió viajar con el Expreso Astral para descubrir la verdad sobre sus orígenes. Por el momento ha preparado para sí misma 67 versiones distintas de la historia de su vida.',25),(26,'Miembro de los Cazadores de Estelaron y hacker experta. Para ella, el universo no es más que un juego de simulación con el que entretenerse. Domina la habilidad de la \"edición de éter\", con la que se pueden modificar los datos de la realidad.',26),(27,'Nació en el Yaoqing de Xianzhou y la enviaron a los Nimbocaballeros del Luofu para recibir entrenamiento militar. Blande la espada de su familia, un regalo de su madre, y ansía el futuro que ella misma ha escrito.',27),(28,'Aeroembajadora de la Comisión del Transporte Celeste del Luofu de Xianzhou. Viaja con delegados empresariales y crea relaciones comerciales y alianzas con muchos mundos.',28),(29,'Un joven que embarcó en el Expreso Astral. Decidió viajar con el Expreso Astral para eliminar las amenazas provocadas por el Estelaron.',29),(30,'Un joven que embarcó en el Expreso Astral. Decidió viajar con el Expreso Astral para eliminar las amenazas provocadas por el Estelaron.',30),(31,'Un joven que embarcó en el Expreso Astral. Decidió viajar con el Expreso Astral para eliminar las amenazas provocadas por el Estelaron.',31),(32,'Un joven que embarcó en el Expreso Astral. Decidió viajar con el Expreso Astral para eliminar las amenazas provocadas por el Estelaron.',32),(33,'Un miembro veterano de la tripulación. La sangre apasionada de su corazón bulle esperando otra larga aventura. A veces dibuja sus vivencias en un cuaderno.',33),(34,'Guardaespaldas del general Jing Yuan. A pesar de su corta edad, es un espadachín sin rival. Nadie puede vencerle cuando tiene una espada en la mano.',34),(35,'Responsable de la Comisión del Transporte Celeste del Luofu de Xianzhou. Yukong era una piloto y arquera con mucha experiencia. Desde que está a cargo de la Comisión, se pasa el día lidiando con toda clase de papeleo.',35),(36,'Un clásico caballero de los Caballeros de la Belleza. Noble, cándido y franco. Viaja solo por el cosmos, siguiendo firmemente el camino de la Belleza.\n\nDefender el honor de la Belleza en el universo es el deber de Argenti. Para cumplir con esta responsabilidad, debe entablar combate con piedad y hacer que el adversario admita la derrota tras golpear con su lanza.',36),(37,'Una Guardamemorias del Jardín de los Recuerdos. Una adivina misteriosa y elegante.',37),(38,'Erudito directo y egocéntrico de la Sociedad del Conocimiento. Suele ocultar su rostro con una extraña cabeza de escayola.\n\nHa demostrado un gran talento desde niño, pero ahora se autodenomina \"mediocre\".',38),(39,'Una foránea que acabó residiendo en Xianzhou por accidente y ahora es una apasionada artista callejera. Aunque su verdadero nombre es Guinevere, Guinaifen es el nombre de Xianzhou que le puso su buena amiga Sushang.',39),(40,'Una de los jueces de la Comisión de los Diez Líderes del Luofu de Xianzhou. De las cuatro funciones de un juez (detener, encerrar, sentenciar e interrogar), Hanya se encarga de la última. Está especializada en leer el karma y los pecados de los criminales. Luego, registra sus delitos y castigos con un pincel oracular.',40),(41,'Una muchacha raposiana lastimosa e indefensa que también es aprendiz de juez en la Comisión de los Diez Líderes y que debe atrapar fantasmas a pesar de tenerles miedo. Huohuo carga con el título de maldita porque un juez de la Comisión de los Diez Líderes selló en su cola un heliobus, el cual responde al nombre de \"Cola\".',41),(42,'Antigua maestra de espadas del Luofu y responsable de la reputación de invencibilidad de los Nimbocaballeros.\nEn la actualidad, su nombre se ha borrado de todos los registros y se ha convertido en una traidora a Xianzhou que camina por la delgada línea que hay entre la cordura y la posesión por Mara.',42),(43,'Un adorable y atento botones del Hotel Fantasía. Sueña con convertirse en un aventurero intergaláctico igual que su abuelo.',43),(44,'Una erudita de carácter gentil y elegante. Es el miembro n.º 81 del Círculo de Genios, experta en el campo de las ciencias de la vida.',44),(45,'Topaz, la experta del Departamento de Inversiones Estratégicas de la Corporación para la Paz Interastral y líder del equipo especial de fiscalización de deudas.',45),(46,'Una de los jueces de la Comisión de los Diez Líderes del Luofu de Xianzhou. ',46),(47,'Es uno de los siete generales celestiales de los Nimbocaballeros de la Alianza Xianzhou y uno de los Seis Aurigas del Luofu de Xianzhou. Una vez devastó los campos de batalla, pero ahora se preocupa por el estado y los planes para el futuro de los Luofu. Blande la espada de su familia, un regalo de su madre, y ansía el futuro que ella misma ha escrito.',47);
/*!40000 ALTER TABLE `profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `results`
--

DROP TABLE IF EXISTS `results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `results` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `all_characters_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK5s63q5118ty8hjnwa95r9ew1` (`all_characters_id`),
  CONSTRAINT `FK5s63q5118ty8hjnwa95r9ew1` FOREIGN KEY (`all_characters_id`) REFERENCES `all_characters` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `results`
--

LOCK TABLES `results` WRITE;
/*!40000 ALTER TABLE `results` DISABLE KEYS */;
INSERT INTO `results` VALUES (1,'Arlan','https://honkai-star-rail.fandom.com/es/wiki/Arlan',1),(2,'Asta','https://honkai-star-rail.fandom.com/es/wiki/Asta',1),(3,'Bailu','https://honkai-star-rail.fandom.com/es/wiki/Bailu',1),(4,'Blade','https://honkai-star-rail.fandom.com/es/wiki/Blade',1),(5,'Bronya','https://honkai-star-rail.fandom.com/es/wiki/Bronya',1),(6,'Clara','https://honkai-star-rail.fandom.com/es/wiki/Clara',1),(7,'Dan Heng','https://honkai-star-rail.fandom.com/es/wiki/Dan_Heng',1),(8,'Dan Heng Imbibitor Lunae','https://honkai-star-rail.fandom.com/es/wiki/Dan_Heng_-_Imbibitor_Lunae',1),(9,'Fu Xuan','https://honkai-star-rail.fandom.com/es/wiki/Fu_Xuan',1),(10,'Gepard','https://honkai-star-rail.fandom.com/es/wiki/Gepard',1),(11,'Herta','https://honkai-star-rail.fandom.com/es/wiki/Herta',1),(12,'Himeko','https://honkai-star-rail.fandom.com/es/wiki/Himeko',1),(13,'Hook','https://honkai-star-rail.fandom.com/es/wiki/Hook',1),(15,'Kafka','https://honkai-star-rail.fandom.com/es/wiki/Kafka',1),(16,'Luka','https://honkai-star-rail.fandom.com/es/wiki/Luka',1),(17,'Luocha','https://honkai-star-rail.fandom.com/es/wiki/Luocha',1),(18,'Lynx','https://honkai-star-rail.fandom.com/es/wiki/Lynx',1),(19,'Natasha','https://honkai-star-rail.fandom.com/es/wiki/Natasha',1),(20,'Pela','https://honkai-star-rail.fandom.com/es/wiki/Pela',1),(21,'Quingque','https://honkai-star-rail.fandom.com/es/wiki/Qingque',1),(22,'Sampo','https://honkai-star-rail.fandom.com/es/wiki/Sampo',1),(23,'Seele','https://honkai-star-rail.fandom.com/es/wiki/Seele',1),(24,'Serval','https://honkai-star-rail.fandom.com/es/wiki/Serval',1),(25,'Siete de Marzo','https://honkai-star-rail.fandom.com/es/wiki/Siete_de_Marzo',1),(26,'Silver Wolf','https://honkai-star-rail.fandom.com/es/wiki/Silver_Wolf',1),(27,'Sushang','https://honkai-star-rail.fandom.com/es/wiki/Sushang',1),(28,'Tingyun','https://honkai-star-rail.fandom.com/es/wiki/Tingyun',1),(29,'Caelus (Fisico)','https://honkai-star-rail.fandom.com/es/wiki/Trazacaminos',1),(30,'Stelle (Fisico)','https://honkai-star-rail.fandom.com/es/wiki/Trazacaminos',1),(31,'Caelus (Fuego)','https://honkai-star-rail.fandom.com/es/wiki/Trazacaminos',1),(32,'Stelle (Fuego)','https://honkai-star-rail.fandom.com/es/wiki/Trazacaminos',1),(33,'Welt','https://honkai-star-rail.fandom.com/es/wiki/Welt',1),(34,'Yanqing','https://honkai-star-rail.fandom.com/es/wiki/Yanqing',1),(35,'Yukong','https://honkai-star-rail.fandom.com/es/wiki/Yukong',1),(36,'Argenti','https://honkai-star-rail.fandom.com/es/wiki/Argenti',1),(37,'Cisne Negro','https://honkai-star-rail.fandom.com/es/wiki/Cisne_Negro',1),(38,'Dr. Ratio','https://honkai-star-rail.fandom.com/es/wiki/Dr._Ratio',1),(39,'Guinaifen','https://honkai-star-rail.fandom.com/es/wiki/Guinaifen',1),(40,'Hanya','https://honkai-star-rail.fandom.com/es/wiki/Hanya',1),(41,'Huohuo','https://honkai-star-rail.fandom.com/es/wiki/Huohuo',1),(42,'Jingliu','https://honkai-star-rail.fandom.com/es/wiki/Jingliu',1),(43,'Misha','https://honkai-star-rail.fandom.com/es/wiki/Misha',1),(44,'Ruan Mei','https://honkai-star-rail.fandom.com/es/wiki/Ruan_Mei',1),(45,'Topaz y Conti','https://honkai-star-rail.fandom.com/es/wiki/Topaz_y_Conti',1),(46,'Xueyi','https://honkai-star-rail.fandom.com/es/wiki/Xueyi',1),(47,'Jing Yuan','https://honkai-star-rail.fandom.com/es/wiki/Jing_Yuan',1);
/*!40000 ALTER TABLE `results` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sprites`
--

DROP TABLE IF EXISTS `sprites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sprites` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `card` varchar(255) DEFAULT NULL,
  `splashscreen` varchar(255) DEFAULT NULL,
  `character_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKc4931xo08595uu2xtltne1bb5` (`character_id`),
  CONSTRAINT `FKc4931xo08595uu2xtltne1bb5` FOREIGN KEY (`character_id`) REFERENCES `characters` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sprites`
--

LOCK TABLES `sprites` WRITE;
/*!40000 ALTER TABLE `sprites` DISABLE KEYS */;
INSERT INTO `sprites` VALUES (1,'https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Arlan/Arlan_Card.png','https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Arlan/Arlan_SplashScreen.png',1),(2,'https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Asta/Asta_Card.png','https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Asta/Asta_SplashScreen.png',2),(3,'https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Bailu/Bailu_SplashScreen.png','https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Bailu/Bauilu_Card.png',3),(4,'https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Blade/Blade_Card.png','https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Blade/Blade_SplashScreen.png',4),(5,'https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Bronya/Bronya_Card.png','https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Bronya/Bronya_SplashScreen.png',5),(6,'https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Clara/Clara_Card.png','https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Clara/Clara_SplashScreen.png',6),(7,'https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Dan_Heng/Dan_Heng_Card.png','https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Dan_Heng/Dang_Heng_SplashScreen.png',7),(8,'https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Dan_Heng_Imbibitor_Lunae/Dan%20Heng%20-%20Imbibitor%20Lunar_Card.png','https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Dan_Heng_Imbibitor_Lunae/Dan_Heng_Imbibitor_Lunae_SplashScreen.png',8),(9,NULL,'https://github.com/RaidenNico/Honkai-Star-Rail-Sprites/blob/main/Fu_Xuan/Fu_Xuan_SplashScreen.png',9),(10,'https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Gepard/Gepard_Card.png','https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Gepard/Gepard_SplashScreen.png',10),(11,'https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Herta/Herta_Card.png','https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Herta/Herta_SplashScreen.png',11),(12,'https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Himeko/Himeko_Card.png','https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Himeko/Himeko_SplashScreen.png',12),(13,'https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Hook/Hook_Card.png','https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Hook/Hook_SplashScreen.png',13),(15,'https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Kafka/Kafka_Card.png','https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Kafka/Kafka_SplashScreen.png',15),(16,'https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Luka/Luka_Card.png','https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Luka/Luka_SplashScreen.png',16),(17,'https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Luocha/Luocha_Card.png','https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Luocha/Luocha_SplashScreen.png',17),(18,NULL,'https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Lynx/Lynx_SplashScreen.png',18),(19,'https://github.com/RaidenNico/Honkai-Star-Rail-Sprites/blob/main/Natasha/Natasha_Card.png','https://github.com/RaidenNico/Honkai-Star-Rail-Sprites/blob/main/Natasha/Natasha.png',19),(20,'https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Pela/Pela_Card.png','https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Pela/Pela_SplashScreen.png',20),(21,'https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Quingque/Quingque_Card.png','https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Quingque/Quingque_SplashScreen.png',21),(22,'https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Sampo/Sampo_Card.png','https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Sampo/Sampo_SplashScreen.png',22),(23,'https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Seele/Seele_Card.png','https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Seele/Seele_SplashScreen.png',23),(24,'https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Serval/Serval_Card.png','https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Serval/Serval_SplashScreen.png',24),(25,'https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Siete_de_Marzo/Siete_de_Marzo_Card.png','https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Siete_de_Marzo/Siete_de_Marzo_SplashScreen.png',25),(26,'https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Silver_Wolf/Silver%20Wolf_Card.png','https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Silver_Wolf/Silver_Wolf_SplashScreen.png',26),(27,'https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Sushang/Sushang_Card.png','https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Sushang/Sushang_SplashScreen.png',27),(28,'https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Tingyun/Tingyun_Card.png','https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Tingyun/Tingyun_SplashScreen.png',28),(29,'https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Trazacaminos/Caelus/Physic/Trazacaminos-male_Card.png','https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Trazacaminos/Caelus/Physic/Trazacaminos-male.png',29),(30,NULL,'https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Trazacaminos/Estela/Physic/Trazacaminos-female.png',30),(31,'https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Trazacaminos/Caelus/Fire/Trazacaminos-male-fire_Card.png','https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Trazacaminos/Caelus/Fire/Trazacaminos-male-fire.png',31),(32,NULL,'https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Trazacaminos/Estela/Fire/Trazacaminos-female-fire.png',32),(33,'https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Welt/Welt_Card.png','https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Welt/Welt_SplashScreen.png',33),(34,'https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Yanqing/Yanquing_Card.png','https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Yanqing/Yanqing_SplashScreen.png',34),(35,'https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Yukong/Yukong_Card.png','https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Yukong/Yukong_SplashScreen.png',35),(36,NULL,NULL,36),(37,NULL,NULL,37),(38,NULL,NULL,38),(39,NULL,NULL,39),(40,NULL,NULL,40),(41,NULL,NULL,41),(42,NULL,NULL,42),(43,NULL,NULL,43),(44,NULL,NULL,44),(45,NULL,NULL,45),(46,NULL,NULL,46),(47,'https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Jing_Yuan/Jing%20Yuan_Card.png','https://raw.githubusercontent.com/RaidenNico/Honkai-Star-Rail-Sprites/main/Jing_Yuan/Jing_Yuan_SplashScreen.png',47);
/*!40000 ALTER TABLE `sprites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `story`
--

DROP TABLE IF EXISTS `story`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `story` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `story1` longtext,
  `story2` longtext,
  `story3` longtext,
  `story4` longtext,
  `character_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK2isw7hgl34alhv48a64quj3rr` (`character_id`),
  CONSTRAINT `FK2isw7hgl34alhv48a64quj3rr` FOREIGN KEY (`character_id`) REFERENCES `characters` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `story`
--

LOCK TABLES `story` WRITE;
/*!40000 ALTER TABLE `story` DISABLE KEYS */;
/*!40000 ALTER TABLE `story` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-02-25  1:14:24
