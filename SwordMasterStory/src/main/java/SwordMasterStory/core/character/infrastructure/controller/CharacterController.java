package SwordMasterStory.core.character.infrastructure.controller;

import SwordMasterStory.core.character.infrastructure.model.Character;
import SwordMasterStory.core.character.ports.in.*;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@RestController
@RequestMapping("/home")
public class CharacterController {

    private final CreateCharacterUseCase createCharacterUseCase;

    private final DeleteCharacterUseCase deleteCharacterUseCase;

    private final ListAllCharacterUseCase listAllCharacterUseCase;

    private final ObtainCharacterUseCase obtainCharacterUseCase;

    private final UpdateCharacterUseCase updateCharacterUseCase;

    @PostMapping
    public ResponseEntity<Character> createCharacter(@RequestBody Character character) {
        Character createdCharacter = createCharacterUseCase.createCharacter(character);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdCharacter);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteCharacter(@PathVariable("id") Long id) {
        boolean deleted = deleteCharacterUseCase.deleteCharacter(id);
        if (deleted) {
            return ResponseEntity.ok("Character with ID " + id + " was deleted");
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Could not find or delete character with ID " + id);
        }
    }

    @GetMapping("/list")
    public List<Character> listAllCharacters() {
        return listAllCharacterUseCase.listCharacter();
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getCharacterById(@PathVariable("id") Long id) {
        Optional<Character> characterOptional = obtainCharacterUseCase.obtainCharacter(id);
        if (characterOptional.isPresent()) {
            Character character = characterOptional.get();
            return ResponseEntity.ok(character);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("The character with ID " + id + " does not exist in the database.");
        }
    }


    @PutMapping("/{id}")
    public ResponseEntity<?> updateCharacter(@PathVariable("id") Long id, @RequestBody Character characterDetails) {
        Character updatedCharacter = updateCharacterUseCase.updateCharacter(id, characterDetails);
        if (updatedCharacter != null) {
            return ResponseEntity.ok("Character data updated successfully.");
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error updating character data.");
        }
    }
}
