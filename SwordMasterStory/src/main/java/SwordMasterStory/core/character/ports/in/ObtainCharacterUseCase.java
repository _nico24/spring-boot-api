package SwordMasterStory.core.character.ports.in;

import SwordMasterStory.core.character.infrastructure.model.Character;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public interface ObtainCharacterUseCase {

    Optional<Character> obtainCharacter(Long id);
}
