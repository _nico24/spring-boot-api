package SwordMasterStory.core.character.ports.in;

import SwordMasterStory.core.character.infrastructure.model.Character;
import org.springframework.stereotype.Service;

@Service
public interface UpdateCharacterUseCase {

    Character updateCharacter(Long id, Character character);
}
