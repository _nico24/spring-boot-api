package SwordMasterStory.core.character.ports.in;


import org.springframework.stereotype.Service;

@Service
public interface DeleteCharacterUseCase {

    boolean deleteCharacter(Long id);
}
