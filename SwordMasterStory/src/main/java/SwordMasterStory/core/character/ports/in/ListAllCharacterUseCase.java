package SwordMasterStory.core.character.ports.in;

import SwordMasterStory.core.character.infrastructure.model.Character;
import org.springframework.stereotype.Service;


import java.util.ArrayList;

@Service
public interface ListAllCharacterUseCase {
    ArrayList<Character> listCharacter();
}
