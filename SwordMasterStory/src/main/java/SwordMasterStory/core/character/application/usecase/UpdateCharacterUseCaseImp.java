package SwordMasterStory.core.character.application.usecase;

import SwordMasterStory.core.character.infrastructure.model.Character;
import SwordMasterStory.core.character.ports.in.UpdateCharacterUseCase;
import SwordMasterStory.core.character.repository.CharacterRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class UpdateCharacterUseCaseImp implements UpdateCharacterUseCase {

    private CharacterRepository characterRepository;

    @Override
    public Character updateCharacter(Long id, Character characterDetails) {
        Optional<Character> optionalCharacter = characterRepository.findById(id);
        if (optionalCharacter.isPresent()) {
            Character existingCharacter = optionalCharacter.get();
            // Aplica los cambios de los detalles del personaje
            existingCharacter.setName(characterDetails.getName());
            existingCharacter.setAttribute(characterDetails.getAttribute());
            existingCharacter.setType(characterDetails.getType());
            existingCharacter.setDate(characterDetails.getDate());
            // Guarda el personaje actualizado
            return characterRepository.save(existingCharacter);
        } else {
            // El personaje con el ID dado no fue encontrado
            return null;
        }
    }
}

