package SwordMasterStory.core.character.application.usecase;

import SwordMasterStory.core.character.infrastructure.model.Character;
import SwordMasterStory.core.character.ports.in.CreateCharacterUseCase;
import SwordMasterStory.core.character.repository.CharacterRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CreateCharacterUseCaseImp implements CreateCharacterUseCase {

    private CharacterRepository characterRepository;

    @Override
    public Character createCharacter(Character character) {
        return characterRepository.save(character);
    }
}
