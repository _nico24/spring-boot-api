package SwordMasterStory.core.character.application.usecase;

import SwordMasterStory.core.character.infrastructure.model.Character;
import SwordMasterStory.core.character.ports.in.ObtainCharacterUseCase;
import SwordMasterStory.core.character.repository.CharacterRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class ObtainCharacterUseCaseImp implements ObtainCharacterUseCase {

    private CharacterRepository characterRepository;

    @Override
    public Optional<Character> obtainCharacter(Long id) {
        return characterRepository.findById(id);
    }
}
