package SwordMasterStory.core.character.application.usecase;

import SwordMasterStory.core.character.infrastructure.model.Character;
import SwordMasterStory.core.character.ports.in.ListAllCharacterUseCase;
import SwordMasterStory.core.character.repository.CharacterRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
@AllArgsConstructor
public class ListAllCharacterUseCaseImp implements ListAllCharacterUseCase {

    private CharacterRepository characterRepository;

    @Override
    public ArrayList<Character> listCharacter() {
        return (ArrayList<Character>) characterRepository.findAll();
    }
}
