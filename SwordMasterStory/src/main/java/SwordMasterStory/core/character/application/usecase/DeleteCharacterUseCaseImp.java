package SwordMasterStory.core.character.application.usecase;

import SwordMasterStory.core.character.infrastructure.model.Character;
import SwordMasterStory.core.character.ports.in.DeleteCharacterUseCase;
import SwordMasterStory.core.character.repository.CharacterRepository;
import lombok.AllArgsConstructor;

import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class DeleteCharacterUseCaseImp implements DeleteCharacterUseCase {

    private CharacterRepository characterRepository;

    @Override
    public boolean deleteCharacter(Long id) {
        Optional<Character> optionalCharacter = characterRepository.findById(id);
        if (optionalCharacter.isPresent()) {
            characterRepository.deleteById(id);
            return true; // Personaje eliminado correctamente
        } else {
            return false; // Personaje no encontrado en la base de datos
        }
    }
}
