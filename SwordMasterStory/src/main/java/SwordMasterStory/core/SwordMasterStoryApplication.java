package SwordMasterStory.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SwordMasterStoryApplication {

	public static void main(String[] args) {
		SpringApplication.run(SwordMasterStoryApplication.class, args);
	}

}
