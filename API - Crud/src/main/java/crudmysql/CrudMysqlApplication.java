package crudmysql;

import crudmysql.Students.models.StudentModel;
import crudmysql.Students.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudMysqlApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(CrudMysqlApplication.class, args);
	}

	@Autowired
	private StudentRepository repository;

	@Override
	public void run(String... args) throws Exception {
		/*
		StudentModel student1 = new StudentModel("Nicolas", "Badajos", "nico@gmail.com");
		repository.save(student1);
		StudentModel student2 = new StudentModel("Alexis", "Rojas", "alexis@gmail.com");
		repository.save(student2);
 		*/
	}
}
