package crudmysql.Students.services;

import crudmysql.Students.interfaces.StudentInterface;
import crudmysql.Students.models.StudentModel;
import crudmysql.Students.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentService implements StudentInterface {

    @Autowired
    private StudentRepository studentRepository;

    @Override
    public List<StudentModel> listAllStudent() {
        return studentRepository.findAll();
    }

    @Override
    public StudentModel saveStudent(StudentModel student) {
        return studentRepository.save(student);
    }

    @Override
    public StudentModel obtainStudentForId(Long id) {
        return studentRepository.findById(id).get();
    }

    @Override
    public StudentModel actulizateStudent(StudentModel student) {
        return studentRepository.save(student);
    }

    @Override
    public void eliminateStudent(Long id) {
    studentRepository.deleteById(id);
    }
}
