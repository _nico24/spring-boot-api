package crudmysql.Students.interfaces;

import crudmysql.Students.models.StudentModel;

import java.util.List;

public interface StudentInterface {
    public List<StudentModel> listAllStudent();
    public StudentModel saveStudent(StudentModel student);
    public StudentModel obtainStudentForId(Long id);
    public StudentModel actulizateStudent(StudentModel student);
    public  void eliminateStudent(Long id);
}
