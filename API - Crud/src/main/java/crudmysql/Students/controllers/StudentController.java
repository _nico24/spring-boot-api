package crudmysql.Students.controllers;

import crudmysql.Students.interfaces.StudentInterface;
import crudmysql.Students.models.StudentModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class StudentController {

    @Autowired
    private StudentInterface studentInterface;

    @GetMapping({"/students","/"})
    public String listStudents(Model model){

        model.addAttribute("students", studentInterface.listAllStudent());
        return "students"; // retonar al archivo students
    }

    @GetMapping("/students/new")
    public String showStudentRegisterForm(Model model){
        StudentModel student = new StudentModel();
        model.addAttribute("student", student);
        return "add_student";
    }

    @PostMapping("/students")
    public String saveStudent(@ModelAttribute("student") StudentModel studentModel){
        studentInterface.saveStudent(studentModel);
        return "redirect:/students";
    }

    @GetMapping("/students/edit/{id}")
    public String showEditForm(@PathVariable Long id, Model model){
        model.addAttribute("student", studentInterface.obtainStudentForId(id));
        return "edit_student";
    }

    @PostMapping("/students/{id}")
    public String actulizateStudent(@PathVariable Long id, @ModelAttribute("student") StudentModel studentModel, Model model){
        StudentModel studentExist = studentInterface.obtainStudentForId(id);
        studentExist.setId(id);
        studentExist.setName(studentModel.getName());
        studentExist.setLastname(studentModel.getLastname());
        studentExist.setEmail(studentModel.getEmail());

        studentInterface.actulizateStudent(studentExist);
        return "redirect:/students";
    }

    @GetMapping("/students/{id}")
    public String eliminateStudent(@PathVariable Long id){
        studentInterface.eliminateStudent(id);
        return "redirect:/students";
    }
}
